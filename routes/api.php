<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([

    'middleware' => 'api',

], function ($router) {

    # AUTH
    Route::group(['prefix' => 'auth'], function ($router) {
        Route::post('login', 'AuthController@login');
        Route::post('logout', 'AuthController@logout');
        Route::post('refresh', 'AuthController@refresh');
        Route::post('register', 'AuthController@register');
        Route::post('me', 'AuthController@me');
    });


    # API

    // Busqueda de recursos
    Route::resource('resources', 'Api\OAAController');

    // Obtener recurso OAA
    Route::resource('oaa', 'Api\OAAController');



    // PRUEBAS

    // Weka (Minería de datos)

    
    Route::group(['prefix'=>'test'], function () {
        Route::get('sparql', 'Test\TestsController@sparql');
        Route::get('content', 'Test\TestsController@content');
        Route::get('language', 'Test\TestsController@language');
        Route::get('rdf_graph', 'Test\TestsController@rdf_graph');
        Route::get('weka', 'Test\TestsController@weka');
    });
});

Route::get('logs', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index']);


// Mensaje por defecto de la API para request de paths erroneos
Route::any('/{any}', function () {
    return response()->json([
                'success'=> false,
                'code'=> 404,
                'message'=> 'Not found'
            ], 404);
})->where('any', '.*');
