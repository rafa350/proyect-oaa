<?php

use Illuminate\Support\Facades\DB;

if (!function_exists('artisan')) {


    function artisan()
    {
        return app()->make('Illuminate\Contracts\Console\Kernel');
    } //end artisan()


}

// Cortar una cadena cada $quantity palabras y retornar un array con todas las subcadenas
function words_split($text, $quantity)
{
    return preg_replace('~((?:\S*?\s){' . $quantity . '})~', "$1\n", $text);
} //end words_split()


function characters_split($text, $quantity)
{
    return str_split($text, $quantity);
} //end characters_split()


function characters_split_at_lastword($text, $quantity)
{
    $textArray = str_split($text, $quantity);

    foreach ($textArray as $i => $textFragment) {
        if (isset($textArray[($i + 1)])) {
            $last_char  = substr($textFragment, -1);
            $first_char = substr($textArray[($i + 1)], 1);
            if ($last_char != ' ' && $first_char != ' ') {
                $auxArray            = explode(' ', $textArray[($i + 1)], 2);
                $textFragment        = $textFragment . $auxArray[0];
                $textArray[($i + 1)] = $auxArray[1];
            }
        }
    }

    return $textArray;
} //end characters_split_at_lastword()


function to_object($data)
{
    return json_decode(json_encode($data));
} //end to_object()


function to_array($data)
{
    return json_decode(json_encode($data), true);
} //end to_array()


function indexOfArray($array, $word)
{
    $arrayKeys = array_keys($array, $word);
    $count     = sizeof($arrayKeys);
    return to_object(
        [
            'first' => isset($arrayKeys[0]) ? $arrayKeys[0] : (-1),
            'last'  => ($count > 1) && isset($arrayKeys[($count - 1)]) ? $arrayKeys[($count - 1)] : (-1),
        ]
    );
} //end indexOfArray()


function formatText(string $value = '')
{
    $result = $value;

    $result = preg_replace("/[^A-Za-z0-9?![:space:]]/", " ", $result);

    $result = explode('-', $result);
    $result = implode(' ', $result);

    $result = ucwords($result);

    $result = explode(' ', $result);
    $result = implode('', $result);

    return $result;
} //end formatText()


function in_arrayi($needle, $haystack)
{
    return in_array(strtolower($needle), array_map('strtolower', $haystack));
} //end in_arrayi()


function in_arrayis($needle, $haystack)
{
    if ($haystack) {
        $haystack = md_str_replace(' ', '', $haystack);
        $needle   = str_replace(' ', '', $needle);
        // var_dump([$needle,in_array($needle, $haystack)]);
        return in_array($needle, $haystack);
    }

    return false;
} //end in_arrayis()


function in_array_intersection($needles, $haystack)
{
    foreach ($needles as $key => $needle) {
        if (in_array(strtolower($needle), array_map('strtolower', $haystack))) {
            return true;
        }
    }

    return false;
} //end in_array_intersection()


function in_array_unsensitive_no_spaces($needles, $haystack)
{
    $needles = array_map('strtolower', $needles) ?: [];
    $needles = md_str_replace(' ', '', $needles);

    $haystack = array_map('strtolower', $haystack) ?: [];
    $haystack = md_str_replace(' ', '', $haystack);

    foreach ($needles as $key => $needle) {
        if (in_array($needle, $haystack)) {
            return true;
        }
    }

    return false;
} //end in_array_unsensitive_no_spaces()


function md_str_replace($find, $replace, $array)
{
    // Same result as using str_replace on an array, but does so recursively for multi-dimensional arrays
    if (!is_array($array)) {
        // Used ireplace so that searches can be case insensitive
        return str_ireplace($find, $replace, $array);
    }

    $newArray = [];

    foreach ($array as $key => $value) {
        $newArray[$key] = md_str_replace($find, $replace, $value);
    }

    return $newArray;
} //end md_str_replace()


function set_pgsql_id_secuence($tables)
{
    if ('pgsql' === DB::getDriverName()) {
        foreach ($tables as $table) {
            DB::statement("SELECT setval(pg_get_serial_sequence('{$table}', 'id'), coalesce(max(id)+1,1), false) FROM {$table}");
        }
    }
} //end set_pgsql_id_secuence()

function is_filled($value)
{
    return $value !== null && empty($value) === false;
}//end set_pgsql_id_secuence()
