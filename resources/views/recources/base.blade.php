<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', $metadata->lom_general->idioma) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ $metadata->lom_general->titulo }}</title>

    <style type="text/css">
        .content {
            text-align: justify;
            padding: 60px 50px;
            font-family: sans-serif;
            line-height: 30px;
            white-space: pre;
        }
    </style>

</head>
<body>
    <div class="content">
        {{ $content }}
    </div>
</body>
</html>
