import Vue from "vue";

import App from "~/views/Index.vue";

import router from "~/router";
import store from "~/store";

// i18n
import i18n from "../i18n/i18n";

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

window.onload = function () {
    const app = new Vue({
        router,
        store,
        i18n,
        render: h => h(App)
    }).$mount("#app");
};

const $ = require("jquery");
window.$ = $;

//require("./bootstrap");
