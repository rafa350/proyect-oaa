import M from "minimatch";

export default {

    es: {
        // Subclases de LOM
        general: 'General',
        clasificacion: 'Clasificación',
        educativo: 'Educativo',
        tecnica: 'Técnica',

        // Identificadores
        id: 'Identificador',
        oa_id: 'Identificador',

        // Metadatos de subclase General
        ambito: 'Ámbito',
        titulo: 'Título',
        idioma: 'Idioma',
        descripcion: 'Descripción',
        palabra_clave: 'Palabra clave',

        // Metadatos de subclase Clasificación
        palabras_claves: 'Palabras claves',
        proposito: 'Propósito',
        ruta_tax_entrada: 'Entrada de ruta taxonómica',
        ruta_tax_fuente: 'Fuente de ruta taxonómica',
        ruta_tax_identificador: 'Identificador de ruta taxonómica',

        // Metadatos de subclase Educativo
        contexto_uso: 'Contexto de uso',
        densidad_semantica: 'Densidad semantica',
        destinatario: 'Destinario',
        edad: 'Edad',
        nivel_interactividad: 'Nivel interactividad',
        tiempo_aprendizaje: 'Tipo de recurso aprendizaje',
        tipo_interactividad: 'Tipo de recurso interactividad',
        tipo_recurso_educativo: 'Tipo de recurso educativo',

        // Metadatos de subclase Técnica
        duracion: 'Duración',
        formato: 'Formato',
        localizacion: 'Localización',
        otros_requisitos: 'Otros requisitos de plataforma',
        pautas_instalacion: 'Pautas de instalación',
        tamano: 'Tamaño',

        // Atributos base de MYSQL
        created_at: 'Creado',
        updated_at: 'Actualizado',

        // Otros
        vacio: 'Sin definir',

    },

    en: {

        // Subclases de LOM
        general: 'General',
        clasificacion: 'Classification',
        educativo: 'Educational',
        tecnica: 'Technique',

        // Identificadores
        id: 'Identifier',
        oa_id: 'Identifier',

        // Metadatos de subclase General
        ambito: 'Scope',
        titulo: 'Title',
        idioma: 'Language',
        descripcion: 'Description',
        palabra_clave: 'Keyword',

        // Metadatos de subclase Clasificación
        palabras_claves: 'Keywords',
        proposito: 'Purpose',
        ruta_tax_entrada: 'Taxonomic route input',
        ruta_tax_fuente: 'Taxonomic route source',
        ruta_tax_identificador: 'Taxonomic path identifier',

        // Metadatos de subclase Educativo
        contexto_uso: 'Context of use',
        densidad_semantica: 'Semantic density',
        destinatario: 'Recipient',
        edad: 'Age',
        nivel_interactividad: 'Interactivity level',
        tiempo_aprendizaje: 'Type of learning resource',
        tipo_interactividad: 'Interactivity resource type',
        tipo_recurso_educativo: 'Type of educational resource',

        // Metadatos de subclase Técnica
        duracion: 'Duration',
        formato: 'Format',
        localizacion: 'Location',
        otros_requisitos: 'Other platform requirements',
        pautas_instalacion: 'Installation guidelines',
        tamano: 'Size',

        // Atributos base de MYSQL
        created_at: 'Created',
        updated_at: 'Updated',

        // Otros
        vacio: 'Undefined',

    },

}