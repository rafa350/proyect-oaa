FROM php:7.4-fpm

RUN apt-get update -y && apt-get install -y sendmail libpng-dev
RUN apt-get install -y libzip-dev

RUN docker-php-ext-install mysqli pdo pdo_mysql
RUN docker-php-ext-install zip
RUN docker-php-ext-install gd

# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
# env COMPOSER_ALLOW_SUPERUSER=1
# RUN composer install

# Run chmod -R 777 storage