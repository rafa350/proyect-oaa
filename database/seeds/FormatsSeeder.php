<?php

use Illuminate\Database\Seeder;

use App\{
    FormatByStyle,
    Style
};

class FormatsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('formats_by_style')->delete();

        $global = [
            [
                'format' => 'mp3',
                'style' => 'Aural'
            ],
            [
                'format' => 'aac',
                'style' => 'Aural'
            ],
            [
                'format' => 'html',
                'style' => 'ReadWrite'
            ],
            [
                'format' => 'txt',
                'style' => 'ReadWrite'
            ],
            [
                'format' => 'mp4',
                'style' => 'Visual'
            ],
            [
                'format' => 'stl',
                'style' => 'Kinesthetic'
            ],
        ];

        foreach ($global as $key => $item) {
            $style = Style::where('name',$item['style'])->first();
            FormatByStyle::create([
                'format' => $item['format'],
                'learning_style_id' => $style->id
            ]);
        }
    }
}
