<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\LomEducativo;

class LomEducativoSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table(LomEducativo::BD_TABLE)->delete();

        DB::table('lom_educativo')->insert(
            [
                LomEducativo::ID                        => 1,
                LomEducativo::USE_CONTEXT               => 'educación secundaria',
                LomEducativo::SEMANTIC_DENSITY          => 'alta',
                LomEducativo::ADDRESSEE                 => 'aprendiz',
                LomEducativo::AGE                       => '15',
                LomEducativo::INTERACTIVITY_LEVEL       => 'medio',
                LomEducativo::LEARNING_TIME             => '',
                LomEducativo::INTERACTIVITY_TYPE        => 'video',
                LomEducativo::EDUCATIONAL_RESOURCE_TYPE => 'diapositiva',
            ]
        );

        set_pgsql_id_secuence([LomEducativo::BD_TABLE]);

    }//end run()


}//end class
