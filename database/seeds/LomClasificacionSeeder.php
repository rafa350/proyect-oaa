<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\LomClasificacion;

class LomClasificacionSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table(LomClasificacion::BD_TABLE)->delete();

        LomClasificacion::create(
            [
                LomClasificacion::ID             => 1,
                LomClasificacion::DESCRIPTION    => 'Un instrumento médico para escuchar llamado estetoscopio.',
                LomClasificacion::WORD_KEY_TYPES => 'instrumento de diagnóstico',
                LomClasificacion::PURPOSE        => 'objetivo educativo',
                LomClasificacion::INPUT_TAX      => 'Médecine',
                LomClasificacion::ORIGIN_TAX     => 'ARIADNE',
                LomClasificacion::ID_TAX         => 'BF180',
            ]
        );

        set_pgsql_id_secuence([LomClasificacion::BD_TABLE]);

    }//end run()


}//end class
