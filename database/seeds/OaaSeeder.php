<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Oaa;

class OaaSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table(Oaa::BD_TABLE)->delete();

        $this->call(LomGeneralSeeder::class);
        $this->call(LomClasificacionSeeder::class);
        $this->call(LomEducativoSeeder::class);
        $this->call(LomTecnicaSeeder::class);

        Oaa::create(
            [
                Oaa::ID                   => 1,
                Oaa::LOM_GENERAL_ID       => 1,
                Oaa::LOM_CLASIFICACION_ID => 1,
                Oaa::LOM_EDUCATIVO_ID     => 1,
                Oaa::LOM_TECNICA_ID       => 1,
            ]
        );

        set_pgsql_id_secuence([Oaa::BD_TABLE]);

        // $oaa = factory(App\Oaa::class, 10)->create();

    }//end run()


}//end class
