<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table(User::BD_TABLE)->delete();

        User::create(
            [
                User::ID                => 1,
                User::NAME              => 'Aprendiz',
                User::EMAIL             => 'aprendiz@oa.com',
                User::PASSWORD          => bcrypt('123456789'),
                User::GENDER            => 'D',
                User::BIRTHDATE         => Carbon::now(),
                User::BIRTHPLACE        => 'Desconocido',
                User::STATUS            => true,
                User::EMAIL_VERIFIED_AT => Carbon::now(),
                User::CREATED_AT        => Carbon::now(),
            ]
        );

        set_pgsql_id_secuence([User::BD_TABLE]);

        $this->call(PreferenciasSeeder::class);
        $this->call(LogOaasTableSeeder::class);

    }//end run()


}//end class
