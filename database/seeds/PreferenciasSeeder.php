<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Preferencias;

class PreferenciasSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table(Preferencias::BD_TABLE)->delete();

        Preferencias::create(
            [
                Preferencias::ID         => 1,
                Preferencias::USER_ID    => 1,
                Preferencias::CONTENT    => 'Deportes',
                Preferencias::LANGUAGE   => 'en',
                Preferencias::CREATED_AT => Carbon::now(),
            ]
        );

        set_pgsql_id_secuence([Preferencias::BD_TABLE]);

    }//end run()


}//end class
