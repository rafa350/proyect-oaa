<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{


    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        set_pgsql_id_secuence(['users', 'preferencias', 'lom_general', 'lom_clasificacion', 'lom_educativo', 'lom_tecnica', 'oaas', 'log_oaas']);

        $this->call(LearningStylesSeeder::class);
        $this->call(FormatsSeeder::class);
        
        $this->call(UsersTableSeeder::class);

        set_pgsql_id_secuence(['users', 'preferencias', 'lom_general', 'lom_clasificacion', 'lom_educativo', 'lom_tecnica', 'oaas', 'log_oaas']);



    }//end run()


}//end class
