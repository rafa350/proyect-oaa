<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\LomGeneral;

class LomGeneralSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table(LomGeneral::BD_TABLE)->delete();

        LomGeneral::create(
            [
                LomGeneral::ID          => 1,
                LomGeneral::AMBIT       => 'Siglo XVI en Francia',
                LomGeneral::TITLE       => 'La  vida  y  obra  de  Leonardo daVinci',
                LomGeneral::LANGUAGE    => 'es',
                LomGeneral::DESCRIPTION => 'En este vídeo se presentan brevemente la vida y obra de Leonardo da Vinci. El elemento central es la producción artística, principalmente la Mona Lisa',
                LomGeneral::WORD_KEY    => 'Mona Lisa',
            ]
        );

        if ('pgsql' === DB::getDriverName()) {
            DB::statement("SELECT setval(pg_get_serial_sequence('lom_general', 'id'), coalesce(max(id)+1,1), false) FROM lom_general");
        }//end if

    }//end run()


}//end class
