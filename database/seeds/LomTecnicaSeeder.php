<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\LomTecnica;

class LomTecnicaSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table(LomTecnica::BD_TABLE)->delete();

        LomTecnica::create(
            [
                LomTecnica::ID                      => 1,
                LomTecnica::DURATION                => 'PT1H30M',
                LomTecnica::FORMAT                  => 'text/ html',
                LomTecnica::LOCATION                => 'https://www.youtube.com/watch?v=S82Vfp7oMGo',
                LomTecnica::OTHER_REQUIREMENTS      => 'No',
                LomTecnica::INSTALLATION_GUIDELINES => 'Descomprima  el  fichero  zip  y  abra el fichero index.html en su navegador.',
                LomTecnica::SIZE                    => '4200',
            ]
        );

        set_pgsql_id_secuence([LomTecnica::BD_TABLE]);

    }//end run()


}//end class
