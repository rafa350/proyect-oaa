<?php

use App\Style;
use Illuminate\Database\Seeder;

class LearningStylesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('learning_styles')->delete();

        $styles = ['Visual','Aural','ReadWrite','Kinesthetic'];
        foreach ($styles as $key => $value) {
            Style::create([
                'name' => $value
            ]);
        }
    }
}
