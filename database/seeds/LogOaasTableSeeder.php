<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\User;
use App\LogOaa;

class LogOaasTableSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table(LogOaa::BD_TABLE)->delete();

        User::find(1)->log_oaas()->createMany(factory(LogOaa::class, 150)->make()->toArray());

        set_pgsql_id_secuence([LogOaa::BD_TABLE]);

    }//end run()


}//end class
