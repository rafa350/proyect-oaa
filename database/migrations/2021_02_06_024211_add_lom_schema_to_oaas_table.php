<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLomSchemaToOaasTable extends Migration
{


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'oaas',
            function (Blueprint $table) {
                $table->unsignedBigInteger('lom_general_id')->nullable();
                $table->foreign('lom_general_id')->references('id')->on('lom_general')->onDelete('cascade');

                $table->unsignedBigInteger('lom_clasificacion_id')->nullable();
                $table->foreign('lom_clasificacion_id')->references('id')->on('lom_clasificacion')->onDelete('cascade');

                $table->unsignedBigInteger('lom_educativo_id')->nullable();
                $table->foreign('lom_educativo_id')->references('id')->on('lom_educativo')->onDelete('cascade');

                $table->unsignedBigInteger('lom_tecnica_id')->nullable();
                $table->foreign('lom_tecnica_id')->references('id')->on('lom_tecnica')->onDelete('cascade');
            }
        );

    }//end up()


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'oaas',
            function (Blueprint $table) {
                $table->dropForeign(['lom_general_id']);
                $table->dropColumn(['lom_general_id']);

                $table->dropForeign(['lom_clasificacion_id']);
                $table->dropColumn(['lom_clasificacion_id']);

                $table->dropForeign(['lom_educativo_id']);
                $table->dropColumn(['lom_educativo_id']);

                $table->dropForeign(['lom_tecnica_id']);
                $table->dropColumn(['lom_tecnica_id']);
            }
        );

    }//end down()


}//end class
