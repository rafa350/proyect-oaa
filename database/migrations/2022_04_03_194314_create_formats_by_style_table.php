<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormatsByStyleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formats_by_style', function (Blueprint $table) {
            $table->id();
            $table->string('format');
            $table->unsignedBigInteger('learning_style_id')->nullable();
            $table->foreign('learning_style_id')->references('id')->on('learning_styles')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formats_by_style');
    }
}
