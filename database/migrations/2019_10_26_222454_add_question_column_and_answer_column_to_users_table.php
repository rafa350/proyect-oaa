<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQuestionColumnAndAnswerColumnToUsersTable extends Migration
{


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'users',
            function (Blueprint $table) {
                $table->string('sexo')->nullable();
                $table->timestamp('fecha_nacimiento')->nullable();
                $table->string('lugar_nacimiento')->nullable();
                $table->boolean('estatus')->default(1);
            }
        );

    }//end up()


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'users',
            function (Blueprint $table) {
                $table->dropColumn(['sexo'])->nullable();
                $table->dropColumn(['fecha_nacimiento'])->nullable();
                $table->dropColumn(['lugar_nacimiento'])->nullable();
                $table->dropColumn(['estatus']);
            }
        );

    }//end down()


}//end class
