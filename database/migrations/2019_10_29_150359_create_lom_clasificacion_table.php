<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLomClasificacionTable extends Migration
{


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'lom_clasificacion',
            function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('descripcion')->nullable();
                $table->string('palabras_claves')->nullable();
                $table->string('proposito')->nullable();
                $table->string('ruta_tax_entrada')->nullable();
                $table->string('ruta_tax_fuente')->nullable();
                $table->string('ruta_tax_identificador')->nullable();
                $table->timestamps();
            }
        );

    }//end up()


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lom_clasificacion');

    }//end down()


}//end class
