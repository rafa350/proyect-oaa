<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLomTecnicaTable extends Migration
{


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'lom_tecnica',
            function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('duracion')->nullable();
                $table->string('formato');
                $table->string('localizacion');
                $table->string('otros_requisitos')->nullable();
                $table->string('pautas_instalacion')->nullable();
                $table->string('tamano')->nullable();
                $table->timestamps();
            }
        );

    }//end up()


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lom_tecnica');

    }//end down()


}//end class
