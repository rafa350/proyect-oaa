<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLomEducativoTable extends Migration
{


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'lom_educativo',
            function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('contexto_uso')->nullable();
                $table->string('densidad_semantica')->nullable();
                $table->string('destinatario')->nullable();
                $table->string('edad')->nullable();
                $table->string('nivel_interactividad')->nullable();
                $table->string('tipo_interactividad')->nullable();
                $table->string('tiempo_aprendizaje')->nullable();
                $table->string('tipo_recurso_educativo')->nullable();
                $table->timestamps();
            }
        );

    }//end up()


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lom_educativo');

    }//end down()


}//end class
