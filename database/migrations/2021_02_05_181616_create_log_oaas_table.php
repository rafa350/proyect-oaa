<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogOaasTable extends Migration
{


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'log_oaas',
            function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->unsignedBigInteger('oaa_id')->unsigned();
                $table->foreign('oaa_id')->references('id')->on('oaas')->onUpdate('cascade')->onDelete('cascade');

                $table->unsignedBigInteger('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');

                $table->timestamps();
            }
        );
    } //end up()


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_oaas');
    } //end down()


}//end class
