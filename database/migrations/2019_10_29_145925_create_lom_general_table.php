<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLomGeneralTable extends Migration
{


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'lom_general',
            function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->string('ambito')->nullable();
                $table->string('titulo');
                $table->string('idioma');
                $table->longText('descripcion');
                $table->string('palabra_clave')->nullable();
                $table->timestamps();
            }
        );

    }//end up()


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lom_general');

    }//end down()


}//end class
