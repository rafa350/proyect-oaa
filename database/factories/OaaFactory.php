<?php

/**
 * @var \Illuminate\Database\Eloquent\Factory $factory
 */
use App\Oaa;
use App\LomGeneral;
use App\LomEducativo;
use App\LomClasificacion;
use App\LomTecnica;
use Faker\Generator as Faker;

$factory->define(
    Oaa::class,
    function (Faker $faker) {
        $lom_general       = factory(LomGeneral::class)->create();
        $lom_educativo     = factory(LomEducativo::class)->create();
        $lom_clasificacion = factory(LomClasificacion::class)->create();
        $lom_tecnica       = factory(LomTecnica::class)->create();

        return [
            Oaa::LOM_GENERAL_ID       => $lom_general,
            Oaa::LOM_EDUCATIVO_ID     => $lom_educativo,
            Oaa::LOM_CLASIFICACION_ID => $lom_clasificacion,
            Oaa::LOM_TECNICA_ID       => $lom_tecnica,
        ];
    }
);
