<?php

/**
 * @var \Illuminate\Database\Eloquent\Factory $factory
 */

use App\LomEducativo;
use Faker\Generator as Faker;

$factory->define(
    LomEducativo::class,
    function (Faker $faker) {

        return [
            LomEducativo::USE_CONTEXT               => $faker->word(),
            LomEducativo::SEMANTIC_DENSITY          => array_rand(['high', 'medium', 'low'], 1),
            LomEducativo::ADDRESSEE                 => $faker->word(),
            LomEducativo::AGE                       => rand(5, 100),
            LomEducativo::INTERACTIVITY_LEVEL       => array_rand(['high', 'medium', 'low'], 1),
            LomEducativo::LEARNING_TIME             => rand(1, 24),
            LomEducativo::INTERACTIVITY_TYPE        => $faker->word(),
            LomEducativo::EDUCATIONAL_RESOURCE_TYPE => $faker->word(),
        ];
    }
);
