<?php


/**
 * @var \Illuminate\Database\Eloquent\Factory $factory
 */

use App\LomClasificacion;
use Faker\Generator as Faker;

$factory->define(
    LomClasificacion::class,
    function (Faker $faker) {
        $types           = LomClasificacion::WORD_KEY_TYPES;
        $palabras_claves = [];
        $t = array_rand($types, rand(1, 5));
        $t = gettype($t) == 'array' ? $t : [$t];
        if (gettype($t) == 'array') {
            foreach ($t  as  $value) {
                $palabras_claves[] = mb_convert_encoding($types[$value], 'UTF-8', 'UTF-8');
            }
        }

        return [
            LomClasificacion ::DESCRIPTION => $faker->text(50),
            LomClasificacion ::WORD_KEYS   => json_encode($palabras_claves),
            LomClasificacion ::PURPOSE     => $faker->sentence(3, true),
            LomClasificacion ::INPUT_TAX   => $faker->word(),
            LomClasificacion ::ORIGIN_TAX  => $faker->word(),
            LomClasificacion ::ID_TAX      => $faker->word(),
        ];
    }
);
