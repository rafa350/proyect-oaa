<?php

/**
 * @var \Illuminate\Database\Eloquent\Factory $factory
 */

use App\LogOaa;
use App\User;
use App\Oaa;

use Faker\Generator as Faker;

$factory->define(
    LogOaa::class,
    function (Faker $faker) {
        return [
            LogOaa ::USER_ID => factory(User::class)->create(),
            LogOaa ::OAA_ID  => factory(Oaa::class)->create(),
        ];
    }
);
