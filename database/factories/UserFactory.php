<?php

/**
 * @var \Illuminate\Database\Eloquent\Factory $factory
 */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
    |--------------------------------------------------------------------------
    | Model Factories
    |--------------------------------------------------------------------------
    |
    | This directory should contain each of the model factory definitions for
    | your application. Factories provide a convenient way to generate new
    | model instances for testing / seeding your application's database.
    |
*/

$factory->define(
    User::class,
    function (Faker $faker) {
        return [
            User::NAME              => $faker->name,
            User::EMAIL             => $faker->unique()->safeEmail,
            User::PASSWORD          => bcrypt('123456789'),
            User::GENDER            => array_rand(['M', 'F', 'D'], 1),
            User::BIRTHDATE         => $faker->dateTime(),
            User::BIRTHPLACE        => $faker->country(),
            User::STATUS            => true,
            User::EMAIL_VERIFIED_AT => $faker->dateTime(),
            User::CREATED_AT        => $faker->dateTime(),
            User::REMEMBER_TOKEN    => $faker->md5,
        ];
    }
);
