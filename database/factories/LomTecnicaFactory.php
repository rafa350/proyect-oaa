<?php

/**
 * @var \Illuminate\Database\Eloquent\Factory $factory
 */

use Faker\Generator as Faker;

use App\{
    FormatByStyle,
    LomTecnica
};

$factory->define(
    LomTecnica::class,
    function (Faker $faker) {
// echo json_encode(FormatByStyle::pluck('format')->toArray()) ;
        return [
            LomTecnica::DURATION                => rand(1, 24),
            LomTecnica::LOCATION                => 'https://en.wikipedia.org/wiki/Lorem_ipsum',
            LomTecnica::FORMAT                  => $faker->randomElement(FormatByStyle::pluck('format')->toArray()),
            LomTecnica::OTHER_REQUIREMENTS      => array_rand(['yes', 'no'], 1),
            LomTecnica::INSTALLATION_GUIDELINES => $faker->sentence(10, true),
            LomTecnica::SIZE                    => rand(100, 5000),
        ];
    }
);
