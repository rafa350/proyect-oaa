<?php

/**
 * @var \Illuminate\Database\Eloquent\Factory $factory
 */

use App\LomGeneral;
use Faker\Generator as Faker;

$factory->define(
    LomGeneral::class,
    function (Faker $faker) {

        return [
            LomGeneral::AMBIT       => $faker->sentence(4, true),
            LomGeneral::TITLE       => $faker->sentence(6, true),
            LomGeneral::LANGUAGE    => $faker->languageCode(),
            LomGeneral::DESCRIPTION => $faker->text(),
            LomGeneral::WORD_KEY    => $faker->word(),
        ];
    }
);
