<?php

namespace App\Exports;

use App\Oaa;
use App\LogOaa;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class LogOaaExportForContent implements FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */

    private $user_id = null;
    private $take = null;
    private $class = null;
    private $resource = null;
    private $lomCategory = null;
    private $headings =  [

            // LOM GENERAL
            // 'ambito',
            // 'titulo',
            // 'idioma',
            // 'descripcion',
            'palabra_clave',

            // LOM CLASIFICACIÓN
            // 'descripcion',
            // 'palabras_claves',
            // 'proposito',
            // 'ruta_tax_entrada',
            // 'ruta_tax_fuente',
            // 'ruta_tax_identificador',

            // LOM EDUCATIVO
            // 'contexto_uso',
            // 'densidad_semantica',
            // 'destinatario',
            // 'edad',
            // 'nivel_interactividad',
            // 'tiempo_aprendizaje',
            // 'tipo_interactividad',
            // 'tipo_recurso_educativo',

            // LOM TECNICA
            // 'duracion',
            'formato',
            // 'localizacion',
            // 'otros_requisitos',
            // 'pautas_instalacion',
            // 'tamano',

            // Extra
            // 'ext'

        ];
    private $records = [];
    private $dynamicHeadings = [];


    # Constructor
    public function __construct($user_id, $lomCategory = null, $class = null, $take = 5, $resource = null)
    {
        $this->user_id = $user_id;
        $this->take = $take;
        $this->class = $class;
        $this->resource = to_object($resource);
        $this->lomCategory = $lomCategory;
        $records = LogOaa::where('user_id', $this->user_id)->orderBy('id', 'desc')->take($this->take)->orderBy('id', 'asc')
                            ->with(['oaa','oaa.lom_general','oaa.lom_clasificacion','oaa.lom_educativo','oaa.lom_tecnica'])
                            ->distinct()->get();

        // Tratamiento de Palabras clave
        $this->dynamicHeadings =  $this->getLomClassPalabrasClave($records);
        // Fin - Tratamiento de Palabras clave

        $this->records = $records;
    }

    public function collection()
    {
        $log_oaas =  $this->records;
        $resource = $this->resource;

        // Agregar recurso (?)
        if ($resource) {
            $lomCategory = $this->lomCategory;
            $class = $this->class;
            $resource->$lomCategory->$class = '?'; // Atributo desconocido a predecir

            $log_oaa = new LogOaa();
            $log_oaa->oaa = $resource;
            $log_oaas = $log_oaas->concat([$log_oaa]); // Agregar recurso al final de la lista
        }
        

        return $log_oaas;
    }


    public function map($log_oaa): array
    {
        $data = [

            // LOM GENERAL
            // $log_oaa->oaa->lom_general->ambito,
            // $log_oaa->oaa->lom_general->titulo,
            // $log_oaa->oaa->lom_general->idioma,
            // $log_oaa->oaa->lom_general->descripcion,
            formatText($log_oaa->oaa->lom_general->palabra_clave),

            // LOM CLASIFICACIÓN
            // $log_oaa->oaa->lom_clasificacion->descripcion,
            // $log_oaa->oaa->lom_clasificacion->palabras_claves,
            // $log_oaa->oaa->lom_clasificacion->proposito,
            // $log_oaa->oaa->lom_clasificacion->ruta_tax_entrada,
            // $log_oaa->oaa->lom_clasificacion->ruta_tax_fuente,
            // $log_oaa->oaa->lom_clasificacion->ruta_tax_identificador,

            // LOM EDUCATIVO
            // $log_oaa->oaa->lom_educativo->contexto_uso,
            // $log_oaa->oaa->lom_educativo->densidad_semantica,
            // $log_oaa->oaa->lom_educativo->destinatario,
            // $log_oaa->oaa->lom_educativo->edad,
            // $log_oaa->oaa->lom_educativo->nivel_interactividad,
            // $log_oaa->oaa->lom_educativo->tiempo_aprendizaje,
            // $log_oaa->oaa->lom_educativo->tipo_interactividad,
            // $log_oaa->oaa->lom_educativo->tipo_recurso_educativo,

            // LOM TECNICA
            // $log_oaa->oaa->lom_tecnica->duracion,
            formatText($log_oaa->oaa->lom_tecnica->formato),
            // $log_oaa->oaa->lom_tecnica->localizacion,
            // $log_oaa->oaa->lom_tecnica->otros_requisitos,
            // $log_oaa->oaa->lom_tecnica->otros_requisitos,
            // $log_oaa->oaa->lom_tecnica->pautas_instalacion,
            // $log_oaa->oaa->lom_tecnica->tamano,

            // Extra
            // 'c'.rand(1, 100)
        ];


        // Tratamiento de Palabras clave
        $dynamicHeadings = $this->dynamicHeadings;
        foreach ($this->dynamicHeadings as $key => $heading) {
            $palabras_claves = gettype($log_oaa->oaa->lom_clasificacion->palabras_claves) == 'string' ? json_decode($log_oaa->oaa->lom_clasificacion->palabras_claves) : $log_oaa->oaa->lom_clasificacion->palabras_claves;
            if (in_arrayis($heading, $palabras_claves)) {
                $data[] = 'true';
            } else {
                $data[] = 'false';
            }
        }
        // Fin - Tratamiento de Palabras clave

        $headings = $this->headings;
        $headings = array_merge($headings, $this->dynamicHeadings);

        $arrayKeys = array_keys($headings, $this->class);

        foreach ($arrayKeys as $key => $index) {
            $lastIndex = sizeof($headings) - 1;
            $value = $data[$index];
            $data[$index] = $data[$lastIndex];
            $data[$lastIndex] = $value;
        }
        
        return $data;
    }

    public function headings(): array
    {
        $data = $this->headings;
        
        // Tratamiento de Palabras clave
        $data = array_merge($data, $this->dynamicHeadings);
        // Fin - Tratamiento de Palabras clave

        $headings = $this->headings;

        // Tratamiento de Palabras clave
        $headings = array_merge($headings, $this->dynamicHeadings);
        // Fin - Tratamiento de Palabras clave


        $arrayKeys = array_keys($headings, $this->class);

        foreach ($arrayKeys as $key => $index) {
            $lastIndex = sizeof($headings) - 1;
            $value = $data[$index];
            $data[$index] = $data[$lastIndex];
            $data[$lastIndex] = $value;
        }

        return $data;
    }

    public function getPredictionIndex()
    {
        return sizeof($this->headings());
    }

    private function getLomClassPalabrasClave($records = [])
    {
        $collection = collect($records);
        $collection = $collection->groupBy('oaa.lom_clasificacion.palabras_claves');
        $keys = [];
        foreach ($records as $key => $value) {
            $palabras_claves = gettype($value->oaa->lom_clasificacion->palabras_claves) == 'string' ? json_decode($value->oaa->lom_clasificacion->palabras_claves) : $value->oaa->lom_clasificacion->palabras_claves;
            $keys[] = $palabras_claves;
        }

        $collection = array_merge(...$keys);

        $collection = array_unique($collection);
        $collection = $collection?: [];
        
       

        $collection = md_str_replace(' ', '', $collection);
        return $collection;
    }
}
