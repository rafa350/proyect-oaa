<?php

namespace App;

use App\Enums\Data\DataTypeEnums;
use App\Enums\LogOaaEnums;
use Illuminate\Database\Eloquent\Model;

class LogOaa extends Model implements LogOaaEnums
{

    protected $table = self::BD_TABLE;

    protected $primaryKey = self::ID;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::OAA_ID,
        self::USER_ID,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        self::OAA_ID  => DataTypeEnums::INTEGER_TYPE,
        self::USER_ID => DataTypeEnums::INTEGER_TYPE,
    ];


    public function user()
    {
        return $this->belongsTo(User::class, self::USER_ID, User::ID);

    }//end user()


    public function oaa()
    {
        return $this->hasOne(Oaa::class, Oaa::ID, self::OAA_ID);

    }//end oaa()


}//end class
