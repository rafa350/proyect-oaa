<?php

namespace App\Enums;

interface DbPediaResourceSparqlEnums
{
    const PREFIX = '
        PREFIX     dbpedia-owl:     <http://dbpedia.org/ontology/>
        PREFIX     dbpedia:         <http://dbpedia.org/resource/>
        PREFIX     dbo:             <http://dbpedia.org/ontology/>
        PREFIX     dbp:             <http://dbpedia.org/property/>
        PREFIX     ns5:             <http://dbpedia.org/datatype/>
        PREFIX     ns1:             <http://dbpedia.org/class/yago/>

        PREFIX     bibo:            <http://purl.org/ontology/bibo/>
        PREFIX     cc:              <http://creativecommons.org/ns#>
        PREFIX     cert:            <http://www.w3.org/ns/auth/cert#>
        PREFIX     ctag:            <http://commontag.org/ns#>
        PREFIX     dc:              <http://purl.org/dc/terms/>
        PREFIX     dc11:            <http://purl.org/dc/elements/1.1/>
        PREFIX     dcat:            <http://www.w3.org/ns/dcat#>
        PREFIX     dcterms:         <http://purl.org/dc/terms/>
        PREFIX     doap:            <http://usefulinc.com/ns/doap#>
        PREFIX     exif:            <http://www.w3.org/2003/12/exif/ns#>
        PREFIX     foaf:            <http://xmlns.com/foaf/0.1/>
        PREFIX     geo:             <http://www.w3.org/2003/01/geo/wgs84_pos#>
        PREFIX     gr:              <http://purl.org/goodrelations/v1#>
        PREFIX     grddl:           <http://www.w3.org/2003/g/data-view#>
        PREFIX     ical:            <http://www.w3.org/2002/12/cal/icaltzd#>
        PREFIX     ma:              <http://www.w3.org/ns/ma-ont#>
        PREFIX     og:              <http://ogp.me/ns#>
        PREFIX     org:             <http://www.w3.org/ns/org#>
        PREFIX     owl:             <http://www.w3.org/2002/07/owl#>
        PREFIX     prov:            <http://www.w3.org/ns/prov#>
        PREFIX     qb:              <http://purl.org/linked-data/cube#>
        PREFIX     rdf:             <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX     rdfa:            <http://www.w3.org/ns/rdfa#>
        PREFIX     rdfs:            <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX     rev:             <http://purl.org/stuff/rev#>
        PREFIX     rif:             <http://www.w3.org/2007/rif#>
        PREFIX     rr:              <http://www.w3.org/ns/r2rml#>
        PREFIX     rss:             <http://purl.org/rss/1.0/>
        PREFIX     schema:          <http://schema.org/>
        PREFIX     sd:              <http://www.w3.org/ns/sparql-service-description#>
        PREFIX     sioc:            <http://rdfs.org/sioc/ns#>
        PREFIX     skos:            <http://www.w3.org/2004/02/skos/core#>
        PREFIX     skosxl:          <http://www.w3.org/2008/05/skos-xl#>
        PREFIX     synd:            <http://purl.org/rss/1.0/modules/syndication/>
        PREFIX     v:               <http://rdf.data-vocabulary.org/#>
        PREFIX     vcard:           <http://www.w3.org/2006/vcard/ns#>
        PREFIX     void:            <http://rdfs.org/ns/void#>
        PREFIX     wdr:             <http://www.w3.org/2007/05/powder#>
        PREFIX     wdrs:            <http://www.w3.org/2007/05/powder-s#>
        PREFIX     wot:             <http://xmlns.com/wot/0.1/>
        PREFIX     xhv:             <http://www.w3.org/1999/xhtml/vocab#>
        PREFIX     xml:             <http://www.w3.org/XML/1998/namespace>
        PREFIX     xsd:             <http://www.w3.org/2001/XMLSchema#>
    ';
}//end interface