<?php

namespace App\Enums;

interface LogQueryEnums
{

    const BD_TABLE = 'log_consultas';
    const ID       = 'id';
    const VALUE    = 'valor';
    const LANGUAGE = 'idioma';
    const USER_ID  = 'user_id';
}//end interface
