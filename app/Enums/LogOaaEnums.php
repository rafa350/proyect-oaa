<?php

namespace App\Enums;

interface LogOaaEnums
{

    const BD_TABLE = 'log_oaas';
    const ID       = 'id';
    const OAA_ID   = 'oaa_id';
    const USER_ID  = 'user_id';

}//end interface
