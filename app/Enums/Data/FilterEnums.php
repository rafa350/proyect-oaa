<?php

namespace App\Enums\Data;

interface FilterEnums
{

    const PAGE       = "page";
    const ITEMS      = "items";
    const QUERY      = "query";
    const WITH       = "with";
    const ORDER_KEY  = "order_key";
    const ORDER_TYPE = "order_type";

}//end interface
