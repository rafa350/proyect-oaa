<?php

namespace App\Enums\Data;

interface DataTypeEnums
{

    const INTEGER_TYPE  = "integer";
    const STRING_TYPE   = "string";
    const FLOAT_TYPE    = "float";
    const BOOLEAN_TYPE  = "boolean";
    const DATETIME_TYPE = "datetime";

}//end interface
