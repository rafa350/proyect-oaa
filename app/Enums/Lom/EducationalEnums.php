<?php

namespace App\Enums\Lom;

interface EducationalEnums
{

    const BD_TABLE    = 'lom_educativo';
    const ID          = 'id';
    const USE_CONTEXT = 'contexto_uso';
    const SEMANTIC_DENSITY = 'densidad_semantica';
    const ADDRESSEE        = 'destinatario';
    const AGE   = 'edad';
    const INTERACTIVITY_LEVEL = 'nivel_interactividad';
    const LEARNING_TIME       = 'tiempo_aprendizaje';
    const INTERACTIVITY_TYPE  = 'tipo_interactividad';
    const EDUCATIONAL_RESOURCE_TYPE      = 'tipo_recurso_educativo';
}//end interface
