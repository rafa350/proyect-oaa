<?php

namespace App\Enums\Lom;

interface GeneralEnums
{

    const BD_TABLE    = 'lom_general';
    const ID          = 'id';
    const AMBIT       = 'ambito';
    const TITLE       = 'titulo';
    const LANGUAGE    = 'idioma';
    const DESCRIPTION = 'descripcion';
    const WORD_KEY    = 'palabra_clave';
}//end interface
