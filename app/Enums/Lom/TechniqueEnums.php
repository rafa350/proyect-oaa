<?php

namespace App\Enums\Lom;

interface TechniqueEnums
{

    const BD_TABLE = 'lom_tecnica';
    const ID       = 'id';
    const DURATION = 'duracion';
    const FORMAT   = 'formato';
    const LOCATION = 'localizacion';
    const OTHER_REQUIREMENTS      = 'otros_requisitos';
    const INSTALLATION_GUIDELINES = 'pautas_instalacion';
    const SIZE      = 'tamano';
}//end interface
