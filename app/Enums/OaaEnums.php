<?php

namespace App\Enums;

interface OaaEnums
{

    const BD_TABLE = 'oaas';
    const ID       = 'id';
    const LOM_GENERAL_ID       = 'lom_general_id';
    const LOM_CLASIFICACION_ID = 'lom_clasificacion_id';
    const LOM_EDUCATIVO_ID     = 'lom_educativo_id';
    const LOM_TECNICA_ID       = 'lom_tecnica_id';

}//end interface
