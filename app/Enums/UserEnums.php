<?php

namespace App\Enums;

interface UserEnums
{
    const BD_TABLE = 'users';
    const ID       = 'id';
    const NAME     = 'nombre';
    const EMAIL    = 'email';
    const PASSWORD = 'password';
    const SECURITY_QUESTION = 'pregunta_seguridad';
    const SEQURITY_ANSWER   = 'respuesta_seguridad';
    const GENDER            = 'sexo';
    const BIRTHDATE         = 'fecha_nacimiento';
    const BIRTHPLACE        = 'lugar_nacimiento';
    const STATUS            = 'estatus';
    const REMEMBER_TOKEN    = 'remember_token';
    const EMAIL_VERIFIED_AT = 'email_verified_at';
}//end interface
