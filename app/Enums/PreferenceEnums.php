<?php

namespace App\Enums;

interface PreferenceEnums
{

    const BD_TABLE = 'preferencias';
    const ID       = 'id';
    const USER_ID  = 'user_id';
    const CONTENT  = 'contenido';
    const LANGUAGE = 'idioma';

}//end interface
