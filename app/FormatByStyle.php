<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormatByStyle extends Model
{
    protected $table = 'formats_by_style';
    protected $fillable = [
      'format',
      'learning_style_id'
    ];
}
