<?php

# Esta clase hace la lectura de un OA del repositorio educativo DBPedia
# Interpreta los metadatos de DBPedia dentro de un OA
# Permite la creación de metadados LOM a partir de metadatos DBPedia

namespace App;

use EasyRdf_Graph;
use EasyRdf_Resource;
use EasyRdf_Namespace;
use EasyRdf_TypeMapper;

use App\Oaa;
use App\LomGeneral;
use App\LomClasificacion;
use App\LomEducativo;
use App\LomTecnica;

use Storage;

class DBPedia_OA
{
    private $primaryData = null;
    private $name = null;
    private $uri = null;
    private $resource = null;
    private $resources = [];
    private $langs = [];
    private $storage = [];

    # Constructor de la clase
    public function __construct($oa, $with_uri = null)
    {
        if (is_null($with_uri)) {
            $this->primaryData = json_decode(json_encode($oa));
        }

        $uri = isset($oa->uri) ? $oa->uri : $oa->URI;
        # Apertura el OA rdf
        $graph = new EasyRdf_Graph($with_uri ? $oa : $uri);
        $graph->load();
        # Carga del recurso DBPedia
        //$this->resource = $graph->resourcesMatching('dbo:abstract')[0];

        $dbo = $graph->resourcesMatching('dbo:abstract');
        $dbpedia_owl = $graph->resourcesMatching('dbpedia-owl:abstract');
        $this->resource = empty($dbpedia_owl) ? $dbo[0]:$dbpedia_owl[0];
        
        $this->uri = $this->resource->getUri();

        
        # Nombre
        $this->name = substr($with_uri ? $oa : $uri, strrpos($with_uri ? $oa : $uri, '/') + 1);

        /*# Almacenar RDF
        $this->storage($uri);

        $aux = [];
        foreach ($this->getLocalizacion() as $key => $resource) {
            try {
                # Apertura el OA rdf
                // $aux[$key] = new EasyRdf_Graph( $this->getRDFUri($resource['value']) );
                // $aux[$key]->load();
                // sleep(1);
                // $dbo = $aux[$key]->resourcesMatching('dbo:abstract');
                // $dbpedia_owl = $aux[$key]->resourcesMatching('dbpedia-owl:abstract');
                // $this->resources[$resource['language']] = empty($dbpedia_owl) ? $dbpedia_owl[0]:$dbo[0];

                // sleep(1);

                # Almacenar RDF
                $this->storage($resource['value'], $resource['language']);

            } catch (\Throwable $th) {
                $this->resources[$resource['language']] = $th;
            }
        }*/
    }

    public function array_change_key_case_recursive($arr)
    {
        return array_map(function ($item) {
            if (is_array($item)) {
                $item = array_change_key_case_recursive($item);
            }
            return $item;
        }, array_change_key_case($arr));
    }


    public function getRedirectUri($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); // Must be set to true so that PHP follows any "Location:" header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $a = curl_exec($ch); // $a will contain all headers
        $url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL); // This is what you need, it will return you the last effective URL
        return $url;
    }

    public function storage($uri, $lang = null)
    {
        try {
            $url = $this->getRDFUri($url);
            $folder = $this->name;
            $contents = file_get_contents($uri);
            $name = substr($uri, strrpos($uri, '/') + 1);
            Storage::put($folder.'/'.$name, $contents);
            return true;
        } catch (\Throwable $th) {
            return false;
        }
    }

    public function open($resource)
    {
        $graph = new EasyRdf_Graph($resource['value']);
        $graph->load();
        return response()->json([
            $resource['language'] = $graph->resourcesMatching('dbo:abstract')[0]
        ]);
    }

    public function getRDFUri($url)
    {
        $rdf_uri = "";
        $rdf_uri = str_replace("resource/", "data/", $url);
        $rdf_uri = $rdf_uri.'.rdf';
        return $rdf_uri;
    }

    # Descripción del recurso OA
    public function getDescripcion($lang = null)
    {
        return $this->get('dbo:abstract', 'literal', $lang);
    }

    # Formato(s) del recurso
    public function getFormato()
    {
        try {
            $standar_resource = $this->getLocalizacion('en');
            return array_unique(get_headers($standar_resource, 1)["Content-Type"])[0];
        } catch (\Throwable $th) {
        }
        return null;
    }

    # Idiomas en los que se encuentra el recurso
    public function getIdioma($lang = null)
    {
        if (is_null($lang)) {
            $lang = explode('.', parse_url(strval($this->uri), PHP_URL_HOST))[0];
            $lang = explode('-', $lang)[0];
            $lang = ($lang == 'dbpedia') ? 'en' : $lang;
            return $lang;
        } else {
            return strval($lang);
        }
    }

    # URI donde se encuentra el recurso al que se hace referencia
    public function getLocalizacion($lang = null)
    {
        $arraySearch = $this->getRescursos();
        if (is_null($lang)) {
            return  $arraySearch;
        } else {
            $index = array_search($lang, array_column($arraySearch, 'language'));
            return $arraySearch[$index]['value'];
        }
    }

    public function getFuente($lang = null)
    {
        return $this->get('prov:wasDerivedFrom', 'resource', null);
    }

    # Título del OA
    public function getTitulo($lang = null)
    {
        if (is_null($lang)) {
            $labels = [];
            foreach ($this->resources as $key => $resource) {
                // $labels[$key] = strval($resource->get('skos:prefLabel|rdfs:label|foaf:name|rss:title|dc:title|dc11:title', 'literal', $lang));
            }
            return $this->resources;
        } else {
            return $this->get('skos:prefLabel|rdfs:label|foaf:name|rss:title|dc:title|dc11:title', 'literal', $lang);
        }
    }

    public function getPalabraClave($lang = null)
    {
        $labels = [];
        if (is_null($lang)) {
            $labels = [];
        } else {
            $classes = isset($this->primaryData->classes) ? $this->primaryData->classes : $this->primaryData->Classes;
            
            if ($classes) {
                $classes = isset($classes->Class) ? $classes->Class : $classes;


                $classes = gettype($classes) == 'object' ? json_decode(json_encode($classes), true) : $classes;

                $labels = array_column($classes, 'Label');
                if (($key = array_search('owl#Thing', $labels)) !== false) {
                    array_splice($labels, $key, 1);
                }
            }
            return array_unique($labels);
        }
    }

    public function getPalabrasClave($lang = null)
    {
        $labels = [];

        if (is_null($lang)) {
            $labels = [];
        } else {
            $categories = isset($this->primaryData->categories) ? $this->primaryData->categories : $this->primaryData->Categories->Category;
            
            if ($categories) {
                $categories = isset($categories->Category) ? $categories->Category : $categories;
                $categories = json_decode(json_encode($categories), true);
                $labels =  array_column($categories, 'URI');

                foreach ($labels as $key => $value) {
                    $labels[$key]  = str_replace("http://dbpedia.org/resource/Category:", "", $value);
                }
            }

            return array_unique($labels);
        }
    }

    # Obtener LOM OBJECT
    public function getLOM($lang = null)
    {
        sleep(2);

        # Nuevo OA
        $oa = new OA();

        # LOM General
        $oa->lom_general = new LomGeneral();
        $oa->lom_general->ambito = null;
        $oa->lom_general->titulo = $this->getTitulo($lang);
        $oa->lom_general->idioma = $this->getIdioma($lang);
        $oa->lom_general->descripcion = $this->getDescripcion($lang);
        $oa->lom_general->palabra_clave = isset($this->getPalabraClave($lang)[0]) ? $this->getPalabraClave($lang)[0] : '';

        # LOM Clasificación
        $oa->lom_clasificacion = new LomClasificacion();
        $oa->lom_clasificacion->descripcion = null;
        $oa->lom_clasificacion->palabras_claves = isset($this->getPalabrasClave($lang)[0]) ? $this->getPalabrasClave($lang)[0] : '';
        $oa->lom_clasificacion->proposito = null;
        $oa->lom_clasificacion->ruta_tax_entrada = null;
        $oa->lom_clasificacion->ruta_tax_fuente = null;
        $oa->lom_clasificacion->ruta_tax_identificador = null;

        # LOM Educativo
        $oa->lom_educativo = new LomEducativo();
        $oa->lom_educativo->contexto_uso = null;
        $oa->lom_educativo->densidad_semantica = null;
        $oa->lom_educativo->destinatario = null;
        $oa->lom_educativo->edad = null;
        $oa->lom_educativo->nivel_interactividad = null;
        $oa->lom_educativo->tiempo_aprendizaje = null;
        $oa->lom_educativo->tipo_interactividad = null;
        $oa->lom_educativo->tipo_recurso_educativo = null;

        # LOM Técnica
        $oa->lom_tecnica = new LomTecnica();
        $oa->lom_tecnica->duracion = null;
        $oa->lom_tecnica->formato = $this->getFormato();
        $oa->lom_tecnica->localizacion = $this->getFuente($lang);
        $oa->lom_tecnica->otros_requisitos = null;
        $oa->lom_tecnica->pautas_instalacion = null;
        $oa->lom_tecnica->tamano = null;

        return $oa;
    }

    # URI del recurso según el idioma en elque puede presentarse
    protected function getRescursos()
    {
        $data = [];
        # Almacenar primero el recurso 'en' puesto que los resultados generados por la api están dados en idioma 'en'
        array_push($data, ['language' => 'en', 'value' => $this->uri]);

        # Buscar todos los recursos "copia" en otros idiomas manejados
        $allSameAs = $this->resource->all('owl:sameAs');
        foreach ($allSameAs as $key => $value) {

            # Válidar que sean recursos exclusivamente del repositorio DBPedia
            if (strpos($value, '.dbpedia.org/resource/') !== false && strpos($value, 'wikidata') == false) {
                $lang = explode('.', parse_url(strval($value), PHP_URL_HOST))[0];
                $url = strval($value);

                # Adjuntar idioma y uri del recurso
                array_push($data, ['language' => $lang, 'value' => $url]);
            }
        }
        return $data;
    }

    # Obtener todos los resultados según la propiedad...
    private function all($property, $type = null, $lang = null)
    {
        $data = [];
        $allProperty = $this->resource->all($property, $type, $lang);
        foreach ($allProperty as $key => $value) {
            array_push($data, strval($value));
        }
        return $data;
    }

    # Obtener dato según la propiedad...
    private function get($property, $type = null, $lang = null)
    {
        return strval($this->resource->get($property, $type, $lang));
    }
}

## Add namespaces
EasyRdf_Namespace::set('dbpedia', 'http://dbpedia.org/resource/');
EasyRdf_Namespace::set('dbpedia-owl', 'http://dbpedia.org/ontology/');
EasyRdf_Namespace::set('dbo', 'http://dbpedia.org/ontology/');
EasyRdf_Namespace::set('ns5', 'http://dbpedia.org/datatype/');
EasyRdf_Namespace::set('ns1', 'http://dbpedia.org/class/yago/');
EasyRdf_Namespace::set('dbp', 'http://dbpedia.org/property/');
EasyRdf_TypeMapper::set('dboa:DBPediaOa', 'DBPedia_OA');
