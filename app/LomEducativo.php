<?php

namespace App;

// Esta subclase describe las características educativas o pedagógicas fundamentales del objeto educativo.
// Corresponde a la categpria 5 del standard LOM
use App\Enums\Data\DataTypeEnums;
use App\Enums\Lom\EducationalEnums;
use Illuminate\Database\Eloquent\Model;
use App\Oaa;

class LomEducativo extends Model implements EducationalEnums
{

    protected $table = self::BD_TABLE;

    protected $primaryKey = self::ID;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::USE_CONTEXT,
        self::SEMANTIC_DENSITY,
        self::ADDRESSEE,
        self::AGE,
        self::INTERACTIVITY_LEVEL,
        self::LEARNING_TIME,
        self::INTERACTIVITY_TYPE,
        self::EDUCATIONAL_RESOURCE_TYPE,
    ];


    public function oaa()
    {
        return $this->belongsTo(Oaa::class, 'oaa_id', 'id');

    }//end oaa()


}//end class
