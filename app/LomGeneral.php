<?php

// Esta subclase agrupa la información general que describe el objeto educativo en su conjunto.
// Corresponde a la categoría 1 del standard LOM
namespace App;

use App\Enums\Lom\GeneralEnums;
use Database\Factories\LomGeneralFactory;

use Illuminate\Database\Eloquent\Model;
use App\Oaa;

class LomGeneral extends Model implements GeneralEnums
{

    protected $table = self::BD_TABLE;

    protected $primaryKey = self::ID;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::AMBIT,
        self::TITLE,
        self::LANGUAGE,
        self::DESCRIPTION,
        self::WORD_KEY,
    ];


    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function oaa()
    {
        return $this->belongsTo(Oaa::class);

    }//end oaa()


}//end class
