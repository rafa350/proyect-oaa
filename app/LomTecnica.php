<?php

// Esta subclase describe los requisitos y características técnicas del objeto educativo.
// Corresponde a la categoría 4 del standard LOM
namespace App;

use App\Enums\Lom\TechniqueEnums;
use Illuminate\Database\Eloquent\Model;
use App\Oaa;

class LomTecnica extends Model implements TechniqueEnums
{

    protected $table = self::BD_TABLE;

    protected $primaryKey = self::ID;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::DURATION,
        self::FORMAT,
        self::LOCATION,
        self::OTHER_REQUIREMENTS,
        self::INSTALLATION_GUIDELINES,
        self::SIZE,
    ];


    public function oaa()
    {
        return $this->belongsTo(Oaa::class, 'oaa_id', 'id');

    }//end oaa()


}//end class
