<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

use Illuminate\Validation\ValidationException;

use App\User;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register', 'me', 'logout'] ]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        try {
            $rules = [
                'email' => ['required', 'email'],
                'password' => ['required'],
            ];

            $request->validate($rules);

            $credentials = request(['email', 'password']);
            if (! $token = auth()->attempt($credentials)) {
                return response()->json(['success' => false, 'message' => 'Credenciales erróneas'], 401);
            }
            return $this->respondWithToken($token);
        } catch (ValidationException $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'errors' => $e->errors(),
            ], 500);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me3()
    {
        return $this->respondWithToken(auth()->refresh());
    }
    public function me()
    {
        try {
            return $this->respondWithToken(auth()->refresh());

            // if (! $user = JWTAuth::parseToken()->authenticate()) {
            //     return response()->json(['user_not_found'], 404);
            // }
        } catch (TokenExpiredException $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 500);
        } catch (TokenInvalidException $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 500);
        } catch (JWTException $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 500);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 500);
        }

        return response()->json(compact('user'));
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['success' =>true, 'message' => 'Cerrado de sesión éxitoso'], 200);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    public function register(Request $request)
    {
        //return response()->json(['request' => $request->all()], 500);
        try {
            $rules = [
                'nombre' => ['required', 'min:3', 'max:30'],
                'sexo' => ['required'],
                'email' => ['required', 'email', 'unique:users,email'],
                'password' => ['required', 'confirmed', 'min:8'],
                'password_confirmation' => ['required'],
                'fecha_nacimiento' => ['required'],
                'lugar_nacimiento' => ['required'],
                'pregunta_seguridad' => ['required'],
                'respuesta_seguridad' => ['required'],
                'preferencias' => ['required'],
            ];

            $request->validate($rules);


            $user =  User::create([
                'nombre' => $request->get('nombre'),
                'sexo' => $request->get('sexo'),
                'email' => $request->get('email'),
                'password' => Hash::make($request->get('password')),
                'fecha_nacimiento' =>  $request->get('fecha_nacimiento'),
                'lugar_nacimiento' =>  $request->get('lugar_nacimiento'),
                'pregunta_seguridad' =>  $request->get('pregunta_seguridad'),
                'respuesta_seguridad' =>  $request->get('respuesta_seguridad'),
                'estatus' =>  true,
            ]);

            try {
                $preferencias =  $request->get('preferencias');
                $user->preferencias()->create([
                'idioma' => isset($preferencias['idioma']) ? $preferencias['idioma'] : null,
                'contenido' =>  isset($preferencias['contenido']) ? $preferencias['contenido'] : null,
            ]);
            } catch (\Throwable $th) {
                $user->delete();
                throw $th;
            }
            
            return response()->json([
                'success' => true,
                'message' => 'Se ha registrado con éxito',
                'user' => $user,
            ], 200);
        } catch (ValidationException $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
                'errors' => $e->errors(),
            ], 500);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'success' => true,
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => User::with('preferencias')->find(auth()->user()->id),
        ]);
    }
}
