<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;
use Storage;

use App\{
    Exports\LogOaaExport,
    Exports\LogOaaExportForContent,
    Services\DBPediaLookup,
    Services\DbPediaResourceSparqlService,
    Services\Resource,
    Services\TextRazorAnalyzing,
    LogConsulta,
    LogOaa,
    LomClasificacion,
    LomEducativo,
    LomGeneral,
    LomTecnica,
    Oaa
};

class OAAController extends Controller
{


    /**
     * OAAController constructor.
     *
     * @param App\Services\DbPediaResourceSparqlService $DbPediaResourceSparqlService
     *
     * @return void
     */
    public function __construct(DbPediaResourceSparqlService $DbPediaResourceSparqlService)
    {
        $this->DbPediaResourceSparqlService = $DbPediaResourceSparqlService;
    } //end __construct()


    public function index(Request $request)
    {
        // Respuesta
        // try {
        // Validaciones
        $rules = [
            'q'       => [
                'required',
                'string',
            ],
            'user_id' => [
                'required',
                Rule::exists('users', 'id'),
            ],
        ];
        $request->validate($rules);

        // Declaración de variables
        $query_string = $request->get('q');
        $user_id      = $request->get('user_id');

        // Registro de Log
        $resultsTextRazorAnalyzing = null;
        try {
            $textRazorAnalyzing        = new TextRazorAnalyzing();
            $resultsTextRazorAnalyzing = $textRazorAnalyzing->detectLanguage($query_string) ?: null;
            if (!empty($resultsTextRazorAnalyzing) && isset($resultsTextRazorAnalyzing->languageIsReliable)) {
                $resultsTextRazorAnalyzing = $resultsTextRazorAnalyzing->languageIsReliable ? $resultsTextRazorAnalyzing->language : null;
            }
        } catch (\Throwable $th) {
            $resultsTextRazorAnalyzing = null;
        }

        $log = null;
        $log = LogConsulta::create(
            [
                'valor'   => $query_string,
                'idioma'  => $resultsTextRazorAnalyzing,
                'user_id' => $user_id,
            ]
        );

        // Búsqueda de recursos
        $results  = null;
        $tryCount = 0;
        while ($tryCount <= 10 && $results === null) {
            $results = $this->DbPediaResourceSparqlService->getDbPediaResourceCollection(['q' => $request->get('q')]);
            $tryCount++;
        }

        if (false) {
            // DBPediaLookup est+a en mantenimiento
            $dbpediaLookup        = null;
            $dbpediaLookup        = new DBPediaLookup();
            $resultsDbpediaLookup = $dbpediaLookup->search($query_string) ?: [];

            if (!empty($resultsDbpediaLookup)) {
                $collection = collect($resultsDbpediaLookup);
                $sorted     = $collection->sortByDesc('score');
                $results    = $sorted->values()->all();
            }
        }

        // Respuesta
        return response()->json(
            [
                'success' => true,
                'results' => $results,
                'log'     => $log,
            ]
        );
        // } catch (ValidationException $e) {
        // return response()->json(
        // [
        // 'success' => false,
        // 'message' => $e->getMessage(),
        // 'errors'  => $e->errors(),
        // ],
        // 500
        // );
        // } catch (\Exception $e) {
        // return response()->json(
        // [
        // 'success' => false,
        // 'message' => $e->getMessage(),
        // ],
        // 500
        // );
        // } //end try

    } //end index()


    public function show($id, Request $request)
    {

        // try {
        // Validaciones
        $rules = [
            'user_id' => [
                'required',
                Rule::exists('users', 'id'),
            ],
        ];
        $request->validate($rules);

        // set_time_limit(0);
        // Declaración de variables
        $uri     = "http://dbpedia.org/resource/" . $id;
        $user_id = $request->get('user_id');
        $lom     = null;
        $style    = $request->get('style', null);

        // Modelado de recurso DPedia y estandarización LOM
        $tryCount = 0;
        while ($tryCount <= 10 && $lom === null) {
            $lom = $this->DbPediaResourceSparqlService->showDbPediaResource(['uri' => $uri]);
            $tryCount++;
        }

        // Almacenar OAA inicial
        $resource = $this->storeOaaAtBD($lom);


        //bien se supone que en esta parte es donde se deberia averiguar que estilo de aprendizaje tiene el usuario
        // Obtener preferencias del aprendíz (Minería de Datos vía Weka)
        if (is_filled($style) === false) {
            $storeMinableView = $this->storeMinableView($user_id, 'lom_tecnica', 'formato', 100, $lom);
            $styleResult       = classifiers($storeMinableView->filepath, $storeMinableView->predictionIndex);
        } // Else... Forzar estilo de aprendizaje.
        else {
            $styleResult       = $style;
        }
        //seria hasta aqui

        // Registrar log (historial)
        $log  = LogOaa::create([
            LogOaa::USER_ID => $user_id,
            LogOaa::OAA_ID => $resource->id,
        ]);

        // Tranformación del OAA
        $preferences = [
            'style' => $styleResult,
            'learning_style' => $styleResult, // se supone que aqui iria el estilo de aprendizaje que se calcule
        ];

        //esta funcion se encarga de transformar el objeto y adaptarlo
        $oaa = $this->transformOaaWithLom($preferences, $resource);


        // Respuesta
        return response()->json(
            [
                'success'     => true,
                'preferences' => $preferences,
                'resource'    => $oaa['metadata'],
                'content'     => $oaa['content'],
                'log'         => $log,
            ]
        );
    } //end show()


    private function storeMinableView($user_id, $lomCategory = null, $predictionClass = null, $nInstances = 10, $lom = null)
    {
        $filename     = 'log-oaas-' . $lomCategory . '-' . $predictionClass . '-' . $user_id;
        $filextension = 'csv';
        $filepath     = $user_id . '\\' . $filename . '.' . $filextension;
        // return Excel::store(new LogOaaExport, $user_id . '\\log-oaas-'. $user_id . '.xlsx', 'weka');
        $LogOaaExport = $this->getExportDataClass($user_id, $lomCategory, $predictionClass, $nInstances, $lom);
        Excel::store($LogOaaExport, $filepath, 'weka');
        Log::info(Storage::disk('weka')->path($filepath));

        $csvdata = Storage::disk('weka')->get($filepath);
        $moddata = str_replace('"', '', $csvdata);
        Storage::disk('weka')->put($filepath, $moddata);

        return to_object(
            [
                'filepath'        => $filepath,
                'predictionIndex' => $LogOaaExport->getPredictionIndex(),
            ]
        );

        // return Excel::store(new LogOaaExport, md5($user_id) . '\\' . md5('log-oaas-'.$user_id) . '.csv', 'weka');
    } //end storeMinableView()


    private function getExportDataClass($user_id, $lomCategory, $predictionClass, $nInstances, $lom)
    {
        if ($lomCategory == 'lom_general' && $predictionClass == 'palabra_clave') {
            return new LogOaaExportForContent($user_id, $lomCategory, $predictionClass, $nInstances, $lom);
        } else {
            return new LogOaaExport($user_id, $lomCategory, $predictionClass, $nInstances, $lom);
        }
    } //end getExportDataClass()


    public function storeOaaAtBD($oaa, $id = null)
    {
        $resource = Oaa::find($id);
        if (is_null($resource)) {
            $resource     = new Oaa();
            $record       = Oaa::orderBy('id', 'desc')->first();
            $resource->id = isset($record->id) ? ($record->id + 1) : 1;
            $id           = isset($record->id) ? ($record->id + 1) : 1;
        }

        $resource->save();

        $record          = LomGeneral::orderBy('id', 'desc')->first();
        $lom_general     = new LomGeneral();
        $lom_general->id = isset($record->id) ? ($record->id + 1) : 1;
        $lom_general->ambito        = $oaa->lom_general->ambito;
        $lom_general->titulo        = $oaa->lom_general->titulo;
        $lom_general->idioma        = $oaa->lom_general->idioma;
        $lom_general->descripcion   = $oaa->lom_general->descripcion;
        $lom_general->palabra_clave = $oaa->lom_general->palabra_clave;
        $lom_general->save();

        $record            = LomClasificacion::orderBy('id', 'desc')->first();
        $lom_clasificacion = new LomClasificacion();
        $lom_clasificacion->id          = isset($record->id) ? ($record->id + 1) : 1;
        $lom_clasificacion->descripcion = $oaa->lom_clasificacion->descripcion;
        $lom_clasificacion->palabras_claves  = $oaa->lom_clasificacion->palabras_claves;
        $lom_clasificacion->proposito        = $oaa->lom_clasificacion->proposito;
        $lom_clasificacion->ruta_tax_entrada = $oaa->lom_clasificacion->ruta_tax_entrada;
        $lom_clasificacion->ruta_tax_fuente  = $oaa->lom_clasificacion->ruta_tax_fuente;
        $lom_clasificacion->ruta_tax_identificador = $oaa->lom_clasificacion->ruta_tax_identificador;
        $lom_clasificacion->save();

        $record            = LomEducativo::orderBy('id', 'desc')->first();
        $lom_educativo     = new LomEducativo();
        $lom_educativo->id = isset($record->id) ? ($record->id + 1) : 1;
        $lom_educativo->contexto_uso       = $oaa->lom_educativo->contexto_uso;
        $lom_educativo->densidad_semantica = $oaa->lom_educativo->densidad_semantica;
        $lom_educativo->destinatario       = $oaa->lom_educativo->destinatario;
        $lom_educativo->edad = $oaa->lom_educativo->edad;
        $lom_educativo->nivel_interactividad   = $oaa->lom_educativo->nivel_interactividad;
        $lom_educativo->tiempo_aprendizaje     = $oaa->lom_educativo->tiempo_aprendizaje;
        $lom_educativo->tipo_interactividad    = $oaa->lom_educativo->tipo_interactividad;
        $lom_educativo->tipo_recurso_educativo = $oaa->lom_educativo->tipo_recurso_educativo;
        $lom_educativo->save();

        $record          = LomTecnica::orderBy('id', 'desc')->first();
        $lom_tecnica     = new LomTecnica();
        $lom_tecnica->id = isset($record->id) ? ($record->id + 1) : 1;
        $lom_tecnica->duracion           = $oaa->lom_tecnica->duracion;
        $lom_tecnica->formato            = $oaa->lom_tecnica->formato;
        $lom_tecnica->localizacion       = $oaa->lom_tecnica->localizacion;
        $lom_tecnica->otros_requisitos   = $oaa->lom_tecnica->otros_requisitos;
        $lom_tecnica->pautas_instalacion = $oaa->lom_tecnica->pautas_instalacion;
        $lom_tecnica->tamano = $oaa->lom_tecnica->tamano;
        $lom_tecnica->save();

        $resource->lom_general_id       = $lom_general->id;
        $resource->lom_clasificacion_id = $lom_clasificacion->id;
        $resource->lom_educativo_id     = $lom_educativo->id;
        $resource->lom_tecnica_id       = $lom_tecnica->id;
        $resource->save();

        return Oaa::with(['lom_general', 'lom_clasificacion', 'lom_educativo', 'lom_tecnica'])->find($id);
    } //end storeOaaAtBD()


    public function transformOaaWithLom($requeriments, $oaa)
    {

        $resource = new Resource($oaa);

        // Requerimiento de Idioma
        // if (isset($requeriments['language']) && $requeriments['language']) {
        //     $value = $requeriments['language'];
        //     if (!is_null($value) && !empty($value) && $value) {
        //         // Si existe el requerimiento
        //         $resource->translate($value);
        //     }
        // }

        // Requerimiento de Idioma
        if (isset($requeriments['learning_style']) && $requeriments['learning_style']) {
            $value = $requeriments['learning_style'] ?? 'Aural';
            if (!is_null($value) && !empty($value) && $value) {
                // Si existe el requerimiento
                // throw new Exception(json_encode($value), 1);

                $resource->transform($value);
            }
        }


        $metadata = $resource->getMetadata();

        // Almacenar OAA inicial
        $resource = $this->storeOaaAtBD($metadata, $oaa->id);

        return [
            'metadata' => $resource,
            'content'  => $resource->getStorageHtml(),
        ];
    } //end transformOaaWithLom()


}//end class
