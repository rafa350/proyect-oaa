<?php

namespace App\Http\Controllers\Test;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WekaController extends Controller
{
    public function classifiers(Request $request)
    {
        if (config('app.env') == 'local') {
            $result = classifiers("test\\testbayes.model", "test\\test.csv");
            return response()->json([
                'result' => $result
            ], 200);
        }
    }
    public function dbpedia()
    {
        $endpoint = "http://es.dbpedia.org/sparql";
        $sc = new SparqlClient();
        $sc->setEndpointRead($endpoint);
        //$sc->setMethodHTTPRead("GET");
        // $q = "select *  where {?x ?y ?z.} LIMIT 5";
        // $q = "select distinct ?Concept where {[] a ?Concept} LIMIT 100";
        $q =    "PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>
                PREFIX dcterms: <http://purl.org/dc/terms/>
                SELECT ?torero ?cantante WHERE{
                ?torero rdf:type dbpedia-owl:BullFighter .
                ?torero dbpedia-owl:spouse ?cantante .
                ?cantante dcterms:subject <http://es.dbpedia.org/resource/Categoría:Cantantes_de_coplas>
                }";
        return $rows = $sc->query($q, 'rows');
        $err = $sc->getErrors();
        if ($err) {
            print_r($err);
            throw new Exception(print_r($err, true));
        }

        foreach ($rows["result"]["variables"] as $variable) {
            printf("%-20.20s", $variable);
            echo '|';
        }
        echo "\n";

        foreach ($rows["result"]["rows"] as $row) {
            foreach ($rows["result"]["variables"] as $variable) {
                printf("%-20.20s", $row[$variable]);
                echo '|';
            }
            echo "\n";
        }
    }
}
