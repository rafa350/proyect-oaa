<?php

namespace App\Http\Controllers\Test;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \BorderCloud\SPARQL\SparqlClient;
use App\Services\Resource;

use App\Exports\LogOaaExport;
use App\Exports\LogOaaExportForContent;
use Maatwebsite\Excel\Facades\Excel;


use App\User;

use App\Oaa;
use App\LogOaa;
use App\LomClasificacion;
use App\LomGeneral;
use App\LomTecnica;
use Storage;

class TestsController extends Controller
{
    public function sparql()
    {
        $endpoint = "http://es.dbpedia.org/sparql";
        $sc = new SparqlClient();
        $sc->setEndpointRead($endpoint);
        //$sc->setMethodHTTPRead("GET");
        // $q = "select *  where {?x ?y ?z.} LIMIT 5";
        // $q = "select distinct ?Concept where {[] a ?Concept} LIMIT 100";

        $q =    "
                PREFIX rec: <http://127.0.0.1:8000/oaa_ontology.owl#>
                PREFIX owl: <http://www.w3.org/2002/07/owl#>
                PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
                PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                PREFIX foaf: <http://xmlns.com/foaf/0.1/>
                PREFIX dc: <http://purl.org/dc/elements/1.1/>
                PREFIX dbpedia2: <http://dbpedia.org/property/>
                PREFIX dbpedia: <http://dbpedia.org/>
                PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
                PREFIX dcterms: <http://purl.org/dc/terms/>
                select distinct ?Concept where {[] a ?Concept} LIMIT 10";
        return $rows = $sc->query($q, 'rows');
        $err = $sc->getErrors();
        if ($err) {
            print_r($err);
            throw new Exception(print_r($err, true));
        }

        foreach ($rows["result"]["variables"] as $variable) {
            printf("%-20.20s", $variable);
            echo '|';
        }
        echo "\n";

        foreach ($rows["result"]["rows"] as $row) {
            foreach ($rows["result"]["variables"] as $variable) {
                printf("%-20.20s", $row[$variable]);
                echo '|';
            }
            echo "\n";
        }
    }

    public function content()
    {
        // //set_time_limit(0);

        $requeriments = [ // Requerimientos ficticios
            'topic' => ['work', 'programming language', 'Person', 'Software'],
        ];

         $lom =  Oaa::with(['lom_general', 'lom_clasificacion', 'lom_educativo', 'lom_tecnica'])        
        ->whereHas('lom_general')
        ->whereHas('lom_clasificacion')
        ->whereHas('lom_educativo')
        ->whereHas('lom_tecnica')
        ->first();

        LomTecnica::find($lom->lom_tecnica_id)->update(['formato' => 'html']);
        $lom->fresh();

        $resource = new Resource($lom);
        $resource->contentTopic($requeriments['topic']);
        return response()->json([
            'metadata' => $resource->getMetadata(),
            'content' => $resource->getContent(),
        ]);
    }

    public function language()
    {
        //set_time_limit(0);

        $requeriments = [ // Requerimientos ficticios
            'language' => 'ES',
        ];

        $lom =  Oaa::with(['lom_general', 'lom_clasificacion', 'lom_educativo', 'lom_tecnica'])
        ->whereHas('lom_general')
        ->whereHas('lom_clasificacion')
        ->whereHas('lom_educativo')
        ->whereHas('lom_tecnica')
        ->first();
        // LomGeneral::where('idioma', 'en')->update(
        //     [
        //     'idioma' => 'es'
        //     ]
        // );
        $resource = new Resource($lom);

        $resource->translate($requeriments['language']);


        return response()->json([
            'metadata' => $resource->getMetadata(),
            'content' => $resource->getContent(),
        ]);
    }

    public function rdf_graph()
    {
        //set_time_limit(0);

        $requeriments = [ // Requerimientos ficticios
            'language' => 'es',
        ];

        $lom =  Oaa::with(['lom_general', 'lom_clasificacion', 'lom_educativo', 'lom_tecnica'])->first();

        $graph = $lom->buildingRDFGraph();
        $content = $lom->getStorageHtml();

        // print $graph;

        return response()->json([
            'graph' => $graph,
            'content' => $content,
        ]);
    }
    public function weka()
    {
        //set_time_limit(0);

        $lom =  Oaa::with(['lom_general', 'lom_clasificacion', 'lom_educativo', 'lom_tecnica'])
        ->whereHas('lom_general')
        ->whereHas('lom_clasificacion')
        ->whereHas('lom_educativo')
        ->whereHas('lom_tecnica')
        ->first();
        // LomClasificacion::where('palabras_claves', 'like', '%President%')->update(
        //     [
        //     'palabras_claves' => '["Programming Language"]'
        //     ]
        // );
        // LomGeneral::where('idioma', 'en')->update(
        //     [
        //     'idioma' => 'es'
        //     ]
        // );

        $storeMinableView = $this->storeMinableView(1, 'lom_general', 'idioma', 99, $lom);
        $langResult = classifiers($storeMinableView->filepath, $storeMinableView->predictionIndex);

            
        $storeMinableView = $this->storeMinableView(1, 'lom_general', 'palabra_clave', 99, $lom);
        $contentResult = selectAttributes($storeMinableView->filepath, $storeMinableView->predictionIndex);


        return response()->json([
            'language' => $langResult,
            'content' => $contentResult,
        ]);
    }

    
    private function storeMinableView($user_id, $lomCategory = null, $predictionClass = null, $nInstances = 10, $lom = null)
    {
        $filename = 'log-oaas-'. $lomCategory .'-'. $predictionClass .'-'. $user_id;
        $filextension = 'csv';
        $filepath = $user_id . '\\' . $filename . '.' . $filextension;
        $filepath =  $filename . '.' . $filextension;
        // return Excel::store(new LogOaaExport, $user_id . '\\log-oaas-'. $user_id . '.xlsx', 'weka');
        
        $LogOaaExport = $this->getExportDataClass($user_id, $lomCategory, $predictionClass, $nInstances, $lom);
        Excel::store($LogOaaExport, $filepath, 'weka');

        $csvdata = Storage::disk('weka')->get($filepath);
        $moddata = str_replace('"', '', $csvdata);
        Storage::disk('weka')->put($filepath, $moddata);

        return to_object([
           'filepath' => $filepath,
           'predictionIndex' => $LogOaaExport->getPredictionIndex(),
        ]);

        // return Excel::store(new LogOaaExport, md5($user_id) . '\\' . md5('log-oaas-'.$user_id) . '.csv', 'weka');
    }

    private function getExportDataClass($user_id, $lomCategory, $predictionClass, $nInstances, $lom)
    {
        if ($lomCategory == 'lom_general' && $predictionClass == 'palabra_clave') {
            return new LogOaaExportForContent($user_id, $lomCategory, $predictionClass, $nInstances, $lom);
        } else {
            return new LogOaaExport($user_id, $lomCategory, $predictionClass, $nInstances, $lom);
        }
    }
}
