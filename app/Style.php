<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Style extends Model
{
    protected $table = 'learning_styles';


    public function formats()
    {
        return $this->hasMany(FormatByStyle::class,'learning_style_id');
    }

}
