<?php

namespace App;

use App\Enums\Data\DataTypeEnums;
use App\Enums\OaaEnums;
use Illuminate\Support\Facades\Storage;
use Database\Factories\OaaFactory;

use Illuminate\Database\Eloquent\Model;
use App\LomGeneral;
use App\LomClasificacion;
use App\LomEducativo;
use App\LomTecnica;



use EasyRdf_Graph;
use EasyRdf_Resource;
use EasyRdf_Namespace;
use EasyRdf_TypeMapper;

// Add namespaces
EasyRdf_Namespace::set('lom', 'http://ltsc.ieee.org/2002/09/lom-base#');
EasyRdf_Namespace::set('lom_gen', 'http://ltsc.ieee.org/2002/09/lom-general#');
EasyRdf_Namespace::set('lom-life', 'http://ltsc.ieee.org/2002/09/lom-lifecycle#');
EasyRdf_Namespace::set('lom-meta', 'http://ltsc.ieee.org/2002/09/lom-metametadata#');
EasyRdf_Namespace::set('lom-tech', 'http://ltsc.ieee.org/2002/09/lom-technical#');
EasyRdf_Namespace::set('lom-edu', 'http://ltsc.ieee.org/2002/09/lom-educational#');
EasyRdf_Namespace::set('lom-rights', 'http://ltsc.ieee.org/2002/09/lom-rights#');
EasyRdf_Namespace::set('lom-rel', 'http://ltsc.ieee.org/2002/09/lom-relation#');
EasyRdf_Namespace::set('lom-ann', 'http://ltsc.ieee.org/2002/09/lom-annotation#');
EasyRdf_Namespace::set('', 'http://ltsc.ieee.org/2002/09/lom-classification#');

class Oaa extends Model implements OaaEnums
{

    protected $table = self::BD_TABLE;

    protected $primaryKey = self::ID;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::LOM_GENERAL_ID,
        self::LOM_CLASIFICACION_ID,
        self::LOM_EDUCATIVO_ID,
        self::LOM_TECNICA_ID,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        self::LOM_GENERAL_ID       => DataTypeEnums::INTEGER_TYPE,
        self::LOM_CLASIFICACION_ID => DataTypeEnums::INTEGER_TYPE,
        self::LOM_EDUCATIVO_ID     => DataTypeEnums::INTEGER_TYPE,
        self::LOM_TECNICA_ID       => DataTypeEnums::INTEGER_TYPE,
    ];


    public function lom_general()
    {
        return $this->hasOne(LomGeneral::class, LomGeneral::ID, self::LOM_GENERAL_ID);

    }//end lom_general()


    public function lom_clasificacion()
    {
        return $this->hasOne(LomClasificacion::class, LomClasificacion::ID, self::LOM_CLASIFICACION_ID);

    }//end lom_clasificacion()


    public function lom_educativo()
    {
        return $this->hasOne(LomEducativo::class, LomEducativo::ID, self::LOM_EDUCATIVO_ID);

    }//end lom_educativo()


    public function lom_tecnica()
    {
        return $this->hasOne(LomTecnica::class, LomTecnica::ID, self::LOM_TECNICA_ID);

    }//end lom_tecnica()


    public function buildingRDFGraph()
    {
        $lang = $this->lom_general->idioma;

        // Start building up a RDF graph
        $lom = new EasyRdf_Graph(env('FRONT_URL').'/resource/'.$this->lom_general->titulo);

        $general       = $lom->resource('#LOM_General', [':lom_general']);
        $tecnica       = $lom->resource('#LOM_Tecnica', [':lom_tecnica']);
        $clasificacion = $lom->resource('#LOM_Clasificacion', [':lom_clasificacion']);
        $educativo     = $lom->resource('#LOM_Educativo', [':lom_educativo']);

        $lom_gen = '';
        $lom_cls = '';
        $lom_edu = '';
        $lom_tec = '';

        // if ($this->lom_general->titulo) {
        // $easyrdf->addLiteral('dc:title', $this->lom_general->titulo, $lang);
        // }
        // if ($this->lom_general->idioma) {
        // $easyrdf->addLiteral('dc:language', $this->lom_general->idioma, $lang);
        // }
        // if ($this->lom_general->descripcion) {
        // $easyrdf->addLiteral('dc:description', $this->lom_general->descripcion, $lang);
        // }
        // if ($this->lom_tecnica->formato) {
        // $easyrdf->addLiteral('dc:format', $this->lom_tecnica->formato, $lang);
        // }
        // if ($lang) {
        // $easyrdf->addLiteral('rdf:type', 'Lecture', $lang);
        // }
        if ($this->lom_general->id) {
            $general->addLiteral($lom_gen.':LOM_General_Identificador', $this->lom_general->id);
        }

        if ($this->lom_general->ambito) {
            $general->addLiteral($lom_gen.':LOM_General_Ambito', $this->lom_general->ambito, $lang);
        }

        if ($this->lom_general->titulo) {
            $general->addLiteral($lom_gen.':LOM_General_Titulo', $this->lom_general->titulo, $lang);
        }

        if ($this->lom_general->idioma) {
            $general->addLiteral($lom_gen.':LOM_General_Idioma', $this->lom_general->idioma, $lang);
        }

        if ($this->lom_general->descripcion) {
            $general->addLiteral($lom_gen.':LOM_General_Descripcion', $this->lom_general->descripcion, $lang);
        }

        if ($this->lom_general->palabra_clave) {
            $general->addLiteral($lom_gen.':LOM_General_PalabraClave', $this->lom_general->palabra_clave, $lang);
        }

        if ($this->lom_clasificacion->id) {
            $clasificacion->addLiteral($lom_cls.':LOM_Clasificacion_Identificador', $this->lom_clasificacion->id);
        }

        if ($this->lom_clasificacion->descripcion) {
            $clasificacion->addLiteral($lom_cls.':LOM_Clasificacion_Descripcion', $this->lom_clasificacion->descripcion, $lang);
        }

        $palabras_claves = gettype($this->lom_clasificacion->palabras_claves) == 'string' ? json_decode($this->lom_clasificacion->palabras_claves) : $this->lom_clasificacion->palabras_claves;
        foreach ($palabras_claves as $key => $value) {
            $clasificacion->addResource($lom_cls.':LOM_Clasificacion_PalabrasClaves', env('FRONT_URL').'/ontology/'.str_replace(' ', '_', $value));
            // $easyrdf->addLiteral('dc:subject', $value, $lang);
            // $lom->resource(env('APP_URL').'/resource/'.$value, ':Taxonomy');
        }

        if ($this->lom_clasificacion->proposito) {
            $clasificacion->addLiteral($lom_cls.':LOM_Clasificacion_Proposito', $this->lom_clasificacion->proposito, $lang);
        }

        if ($this->lom_educativo->id) {
            $educativo->addLiteral($lom_edu.':LOM_Educativo_Identificador', $this->lom_educativo->id);
        }

        // if ($this->lom_educativo->contexto_uso) {
        // $educativo->addLiteral($lom_edu . ':Lecture', $this->lom_educativo->contexto_uso, $lang);
        // }
        // if ($this->lom_educativo->densidad_semantica) {
        // $educativo->addLiteral($lom_edu . ':semanticDensity', $this->lom_educativo->densidad_semantica);
        // }
        // if ($this->lom_educativo->destinatario) {
        // $educativo->addLiteral($lom_edu . ':intendedEndUserRole', $this->lom_educativo->destinatario);
        // }
        if ($this->lom_educativo->edad) {
            $educativo->addLiteral($lom_edu.':LOM_Educativo_RangoTipicoEdad', $this->lom_educativo->edad);
        }

        // if ($this->lom_educativo->nivel_interactividad) {
        // $educativo->addLiteral($lom_edu . ':interactivityLevel', $this->lom_educativo->nivel_interactividad);
        // }
        // if ($this->lom_educativo->tiempo_aprendizaje) {
        // $educativo->addLiteral($lom_edu . ':typicalLearningTime', $this->lom_educativo->tiempo_aprendizaje);
        // }
        if ($this->lom_educativo->tipo_interactividad) {
            $educativo->addLiteral($lom_edu.':LOM_Educativo_TipoInteractividad', $this->lom_educativo->tipo_interactividad);
        }

        if ($this->lom_educativo->tipo_recurso_educativo) {
            $educativo->addLiteral($lom_edu.':LOM_Educativo_TipoRecursoEducativo', $this->lom_educativo->tipo_recurso_educativo);
        }

        if ($this->lom_tecnica->id) {
            $tecnica->addLiteral($lom_tec.':LOM_General_Identificador', $this->lom_tecnica->id);
        }

        if ($this->lom_tecnica->duracion) {
            $tecnica->addLiteral($lom_tec.':LOM_Tecnica_Duracion', $this->lom_tecnica->duracion);
        }

        if ($this->lom_tecnica->formato) {
            $tecnica->addLiteral($lom_tec.':LOM_Tecnica_Formato', $this->lom_tecnica->formato);
        }

        if ($this->lom_tecnica->localizacion) {
            $tecnica->addLiteral($lom_tec.':LOM_Tecnica_Localizacion', $this->lom_tecnica->localizacion);
        }

        // if ($this->lom_tecnica->otros_requisitos) {
        // $tecnica->addLiteral($lom_tec . ':otherPlatformRequirements', $this->lom_tecnica->otros_requisitos);
        // }
        // if ($this->lom_tecnica->pautas_instalacion) {
        // $tecnica->addLiteral($lom_tec . ':installationRemarks', $this->lom_tecnica->pautas_instalacion);
        // }
        if ($this->lom_tecnica->tamano) {
            $tecnica->addLiteral($lom_tec.':LOM_Tecnica_Tamaño', $this->lom_tecnica->tamano);
        }

        // $easyrdf->addResource('doap:license', 'http://usefulinc.com/doap/licenses/bsd');
        // $easyrdf->addResource('doap:download-page', 'http://github.com/njh/easyrdf/downloads');
        // $easyrdf->addResource('doap:download-page', 'http://github.com/njh/easyrdf/downloads');
        // $easyrdf->addResource('doap:bug-database', 'http://github.com/njh/easyrdf/issues');
        // $easyrdf->addResource('doap:mailing-list', 'http://groups.google.com/group/easyrdf');
        // $easyrdf->addResource('doap:category', 'http://dbpedia.org/resource/Resource_Description_Framework');
        // $easyrdf->addResource('doap:category', 'http://dbpedia.org/resource/PHP');
        // $easyrdf->addResource('doap:category', 'http://www.dbpedialite.org/things/24131#id');
        // $easyrdf->addResource('doap:category', 'http://www.dbpedialite.org/things/53847#id');
        // $repository = $lom->newBNode('doap:GitRepository');
        // $repository->addResource('doap:browse', 'http://github.com/njh/easyrdf');
        // $repository->addResource('doap:location', 'git://github.com/njh/easyrdf.git');
        // $easyrdf->addResource('doap:repository', $repository);
        // $njh = $lom->resource('http://njh.me/', 'foaf:Person');
        // $njh->addLiteral('foaf:name', 'Nicholas J Humfrey');
        // $njh->addResource('foaf:homepage', 'http://www.aelius.com/njh/');
        // $easyrdf->add('doap:maintainer', $njh);
        // $easyrdf->add('doap:developer', $njh);
        // $easyrdf->add('foaf:maker', $njh);
        // print $lom->serialise('rdfxml');
        return $lom->dump('html');

        return print $this->lom->serialise('rdfxml');

    }//end buildingRDFGraph()


    public function getStorageHtml()
    {
        $folder  = $this->id;
        $content = Storage::disk('public')->get($folder.'/resource.html');
        return $content;

    }//end getStorageHtml()


}//end class
