<?php

namespace App\Logs;

use Illuminate\Database\Eloquent\Model;

class Query extends Model
{
    protected $table = 'query_logs';

    protected $primaryKey = 'id';

    protected $fillable = [
        'sentencia',
        'idioma',
    ];

}
