<?php

namespace App\Services;

use App\Repositories\DbPediaResourceSparqlRepository;
use App\Services\TextRazorAnalyzing;
use App\Oaa;
use App\LomGeneral;
use App\LomClasificacion;
use App\LomEducativo;
use App\LomTecnica;

class DbPediaResourceSparqlService
{

    protected $DbPediaResourceSparqlRepository;


    /**
     * DbPediaResourceSparqlService constructor.
     *
     * @param App\Repositories\DbPediaResourceSparqlRepository $repository
     *
     * @return void
     */
    public function __construct(DbPediaResourceSparqlRepository $DbPediaResourceSparqlRepository)
    {
        $this->DbPediaResourceSparqlRepository = $DbPediaResourceSparqlRepository;
    } //end __construct()


    /**
     * Execute DbPedia Resource Sparql service.
     *
     * @param mixed $data Data
     *
     * @return mixed
     */
    public function getDbPediaResourceCollection($data)
    {
        return $this->DbPediaResourceSparqlRepository->getAll($data);
    } //end getDbPediaResourceCollection()


    // end execute()


    /**
     * Execute DbPedia Resource Sparql service.
     *
     * @param mixed $data Data
     *
     * @return mixed
     */
    public function showDbPediaResource($data)
    {
        $resource = $this->DbPediaResourceSparqlRepository->showDbPediaResource($data);

        if ($resource === null) {
            return null;
        }
        // Nuevo OA
        $oaa = new Oaa();

        // LOM General
        $oaa->lom_general         = new LomGeneral();
        $oaa->lom_general->ambito = null;
        $oaa->lom_general->titulo = mb_convert_encoding($resource['label'] ?? '', 'UTF-8', 'UTF-8');
        $oaa->lom_general->idioma = mb_convert_encoding($resource['lang'] ?? '', 'UTF-8', 'UTF-8');
        $oaa->lom_general->descripcion   = mb_convert_encoding($resource['abstract'] ?? '', 'UTF-8', 'UTF-8');
        $oaa->lom_general->palabra_clave = $this->getKeyword($resource);

        // LOM Clasificación
        $oaa->lom_clasificacion = new LomClasificacion();
        $oaa->lom_clasificacion->descripcion      = null;
        $oaa->lom_clasificacion->palabras_claves  = mb_convert_encoding($resource['keywords'] ?? '', 'UTF-8', 'UTF-8');
        $oaa->lom_clasificacion->proposito        = null;
        $oaa->lom_clasificacion->ruta_tax_entrada = null;
        $oaa->lom_clasificacion->ruta_tax_fuente  = null;
        $oaa->lom_clasificacion->ruta_tax_identificador = null;

        // LOM Educativo
        $oaa->lom_educativo = new LomEducativo();
        $oaa->lom_educativo->contexto_uso       = null;
        $oaa->lom_educativo->densidad_semantica = null;
        $oaa->lom_educativo->destinatario       = null;
        $oaa->lom_educativo->edad = null;
        $oaa->lom_educativo->nivel_interactividad   = null;
        $oaa->lom_educativo->tiempo_aprendizaje     = null;
        $oaa->lom_educativo->tipo_interactividad    = null;
        $oaa->lom_educativo->tipo_recurso_educativo = null;

        $format = 'html';
        try {
            $format = isset($resource['location']) ? (array_unique(get_headers($resource['location'], 1)["Content-Type"])[0] ?? null) : null;
        } catch (\Throwable $th) {
            // throw $th;
        }
        $format = !empty($format) && !is_null($format) ? mb_convert_encoding($format, 'UTF-8', 'UTF-8') : 'html';

        // LOM Técnica
        $oaa->lom_tecnica           = new LomTecnica();
        $oaa->lom_tecnica->duracion = null;
        $oaa->lom_tecnica->formato  = $format;
        $oaa->lom_tecnica->localizacion       = mb_convert_encoding($resource['location'] ?? '', 'UTF-8', 'UTF-8');
        $oaa->lom_tecnica->otros_requisitos   = null;
        $oaa->lom_tecnica->pautas_instalacion = null;
        $oaa->lom_tecnica->tamano = null;

        return $oaa;
    } //end showDbPediaResource()


    // end execute()
    public function getKeyword($resource)
    {

        try {
            $label = $resource['label'];
            $location = $resource['location'];
            $TextRazorClient = new TextRazorAnalyzing();
            $result          = $TextRazorClient->analyzingUri($location);
            $result          = to_object($result);
            $collection      = collect($result->response->entities);

            $sorted  = $collection->whereNotIn('matchedText', [$label])
                ->where('relevanceScore', '>=', 0.75)
                ->sortByDesc(['confidenceScore']);
            $sorted  = $sorted->values()->first();
            $results = $sorted->matchedText;
            return  $results;
        } catch (\Throwable $th) {
            //$th
        }
        return isset($resource['keywords']) ? mb_convert_encoding($resource['keywords'][0] ?? '', 'UTF-8', 'UTF-8') : null;
    } //end getKeyword()


}//end class
