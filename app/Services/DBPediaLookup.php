<?php

# DBPedia Lookup API es un servicio de búsqueda para URI (recursos) de DBpedia por palabras clave relacionadas. Relacionados significa que la etiqueta de un recurso coincide o un texto de anclaje que se usaba con frecuencia en Wikipedia
# Info: https://wiki.dbpedia.org/lookup

namespace App\Services;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Rule;

use App\DBPedia_OA;

class DBPediaLookup
{
    private $client = null;
    private $results_per_page = 10;
    private $api = 'https://lookup.dbpedia.org/api';

    # Constructor
    public function __construct()
    {
        // set_time_limit(0);
    }

    # Buscador
    public function search(string $search = '')
    {

        // Buscar con DBPedia Lookup API
        $dbpediaSearch = $this->dbpediaSearch($search);
        $response = [];

        if ($dbpediaSearch['success']) {
            $response = $dbpediaSearch['results'];
        } else {
            $message = $dbpediaSearch['message']?: "Error Processing Request";
            if (str_contains($message, 'Could not resolve host')) {
                $message ='La consulta de recursos no se encuentra disponible en este momento. Intente más tarde...';
            }
            throw new \Exception($message, 1);
        }

        return $response;
    }

    # Bucador de DBPedia Lookup API (KeywordSearch)
    public function dbpediaKeywordSearch(string $search = '', int $MaxHits = 10)
    {
        try {
            $KeywordSearch = 'http://lookup.dbpedia.org/api/search.asmx/KeywordSearch?MaxHits='.$MaxHits.'&QueryString='.$search;

            $content = file_get_contents($KeywordSearch);
            $json = json_decode(json_encode(simplexml_load_string($content)), true);

            return isset($json['Result']) ? $json['Result'] : [];
        } catch (\Exception $e) {
            return [];
        }
    }

    
    # Bucador de DBPedia Lookup API (Search)
    public function dbpediaSearch(string $query = '', int $MaxHits = 10, string $format = 'JSON')
    {
        try {
            $endpoint = $this->api . '/search';
            $params = array(
                'format' => $format,
                'query' => $query,
                'MaxHits' => $MaxHits,
            );

            $url = $endpoint . '?' . http_build_query($params);

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            $response = json_decode($response, true);

            if ($err) {
                return  [
                    'success' => false,
                    'message' => $err,
                ];
            } elseif (empty($response) || is_null($response)) {
                return  [
                    'success' => false,
                    'message' => 'El respoitorio de recursos (DBPedia) no se encuentra disponible en este momento, intente más tarde...',
                ];
            }
       
            $response = $response['docs']?: [];

            return [
                'success' => true,
                'results' => $response,
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'message' => $e->getMessage(),
            ];
        }
    }
}
