<?php

// La API TextRazor lo ayuda a extraer y comprender quién, qué, por qué de un texto/url
// API: https://www.textrazor.com/
// Plugin: https://github.com/TextRazor/textrazor-php
namespace App\Services;

class TextRazorAnalyzing
{

    private $apiKey = '147d1c21d26d7084d9a168229a86dc51a8ac5d7eccc10fa9dfb19397';

    private $client = null;

    // Constructor
    public function __construct()
    {
        $this->client = \TextRazorSettings::setApiKey($this->apiKey);
    } //end __construct()


    // Detectar idioma predominante en el texto
    public function detectLanguage(string $text = '', bool $forceReliable = false)
    {
        try {
            $textrazor = new \TextRazor();
            $response  = $textrazor->analyze($text);
            return $response && isset($response['response']) ? to_object($response['response']) : null;
        } catch (\Throwable $th) {
            return null;
        }
    } //end detectLanguage()


    // Analizar texto
    public function analyzingText(string $text = '')
    {
        $success   = false;
        $textrazor = new \TextRazor();
        $textrazor->addExtractor('entities');
        // $textrazor->addExtractor('topics');
        $response = false;
        // $textrazor->addExtractor('words');
        $textrazor->addExtractor('spelling');
        $tryCount = 0;
        while ($tryCount <= 10 && !$success) {
            try {
                $response = $textrazor->analyze($text);
                $success  = true;
            } catch (\Throwable $th) {
                $success = false;
            }
            $tryCount++;
        }

        return $response;
    } //end analyzingText()


    // Analizar texto
    public function analyzingUri(string $url = '')
    {
        $textrazor = new \TextRazor();
        $textrazor->addExtractor('entities');

        $response = $textrazor->analyzeUrl($url);

        return $response;
    } //end analyzingUri()


    // Analizar URL
    public function analyzingUrl(string $url = '')
    {
        // try {
        $textrazor = new \TextRazor();

        $textrazor->addExtractor('words');
        $textrazor->addExtractor('entities');

        $textrazor->setCleanupMode('cleanHTML');
        $textrazor->setCleanupReturnCleaned(true);

        $response = $textrazor->analyzeUrl($url);
        $content  = '';
        if (isset($response['response']['sentences'])) {
            foreach ($response['response']['sentences'] as $sentence) {
                // print_r($sentence['entityId'] . PHP_EOL);
                foreach ($sentence['words'] as $word) {
                    $content = $content . ' ' . $word['token'];
                }
            }
        }

        return (['response' => $response]);
        // } catch (\Throwable $th) {
        // return false;
        // }

    } //end analyzingUrl()


    // Obtener contenido en un URL
    /*
        $CleanupMode = 'raw' | El contenido sin procesar se analiza "tal cual", sin preprocesamiento.

        $CleanupMode = 'stripTags' | Todas las etiquetas se eliminan del documento antes del análisis. Esto eliminará todas las etiquetas HTML, XML, pero el contenido de los encabezados y menús permanecerá. Esta es una buena opción para el análisis de páginas HTML que no son documentos de formato largo.

        $CleanupMode = 'cleanHTML' | El HTML se elimina antes del análisis, incluidas las etiquetas, los comentarios y los menús, dejando solo el cuerpo del artículo.
    */
    public function getUrlText(string $url = '', string $CleanupMode = 'cleanHTML')
    {

        // try {
        $textrazor = new \TextRazor();

        $textrazor->setCleanupMode($CleanupMode);
        $textrazor->setCleanupReturnCleaned(true);

        $response = $textrazor->analyzeUrl($url);

        if ($CleanupMode === 'cleanHTML') {
            $i = 1;
            while (strpos($response['response']['cleanedText'], '[' . $i . ']') !== false) {
                $response['response']['cleanedText'] = str_replace(' [' . $i . ']', '', $response['response']['cleanedText']);
                $i = ($i + 1);
            }
        }

        return $response['response']['cleanedText'];

        // } catch (\Throwable $th) {
        return false;
        // }

    } //end getUrlText()


    public function getClassifier()
    {
        $textrazorClassifier = new \ClassifierManager();

        $classifierId = 'test_cats_php';

        try {
            print_r($textrazorClassifier->deleteClassifier($classifierId));
        } catch (Exception $e) {
            // Silently ignore missing classifier for now.
        }

        // Define some new categories and upload them as a new classifier.
        $newCategories = [];
        array_push($newCategories, ['categoryId' => '1', 'query' => 'concept("banking")']);
        array_push($newCategories, ['categoryId' => '2', 'query' => 'concept("health")']);

        $textrazorClassifier->createClassifier($classifierId, $newCategories);

        // Test the new classifier out with an analysis request.
        $textrazor = new \TextRazor();
        $textrazor->addClassifier($classifierId);

        $text     = 'Barclays misled shareholders and the public about one of the biggest investments in the banks history, a BBC Panorama investigation has found.';
        $response = $textrazor->analyze($text);

        return $response['response'];

        // The client offers various methods for manipulating your stored classifier.
        print_r($textrazorClassifier->allCategories($classifierId));

        print_r($textrazorClassifier->deleteClassifier($classifierId));
    } //end getClassifier()


}//end class
