<?php

# Detector de idioma de textos
# API: https://languagelayer.com
# Plugin: https://github.com/BenMorel/LanguageLayer

namespace App\Services;

use Illuminate\Database\Eloquent\Model;
use BenMorel\LanguageLayer\LanguageLayerClient;

class LanguageLayer
{
    private $apiKey = 'a48f905e00d2dbfb7ef433557ebb44c2';
    private $client = null;

    # Constructor
    public function __construct()
    {
        $this->client = new LanguageLayerClient($this->apiKey);

    }

    # Detectar todos los idiomas en el texto
    public function detectLanguages(string $text = ''){
        try {
            return $this->client->detectLanguages($text);
        } catch (\Throwable $th) {
            return null;
        }
    }

    # Detectar idioma predominante en el texto
    public function detectLanguage(string $text = '', bool $forceReliable = false){
        // try {
            return $this->client->detectLanguage($text);
        // } catch (\Throwable $th) {
        //     return null;
        // }
    }
}
