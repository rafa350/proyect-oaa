<?php

# DBPedia Lookup API es un servicio de búsqueda para URI (recursos) de DBpedia por palabras clave relacionadas. Relacionados significa que la etiqueta de un recurso coincide o un texto de anclaje que se usaba con frecuencia en Wikipedia
# Info: https://wiki.dbpedia.org/lookup

namespace App\Services;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Rule;



use EasyRdf_Graph;
use EasyRdf_Resource;
use EasyRdf_Namespace;
use EasyRdf_TypeMapper;

use App\DBPedia_OA;


use App\Oaa;
use App\LomGeneral;
use App\LomClasificacion;
use App\LomEducativo;
use App\LomTecnica;


use App\Services\TextRazorAnalyzing;

class DBPediaResource
{
    public $uri = null; // URI del recurso
    public $rdfUri = null; // URI del recurso
    private $resources = []; // URI del recurso
    private $foaf = null; // Grafo
    private $api = 'https://lookup.dbpedia.org/api'; //  URI de la api de DBPedia Lookup

    # Constructor
    public function __construct(string $uri)
    {
        // set_time_limit(0);

        $this->uri = $uri;
        $this->rdfUri = $this->getRDFUri($this->uri);
        $this->foaf = $this->openResource();
        $this->resources = $this->foaf->resources();
    }

    # Apertura el Oaa rdf
    public function openResource()
    {
        // try {
        $foaf = new EasyRdf_Graph($this->rdfUri);
        $foaf->load();
        return $foaf;
        // } catch (\Exception $e) {
        //     $message =  $e->getMessage()?: 'Error tratando de obtener el recurso desde DBPedia';
        //     throw new \Exception($message, 1);
        // }
    }

    // Generar enlace del rdf del recurso
    public function getRDFUri($url)
    {
        $rdf_uri = "";
        $rdf_uri = str_replace("resource/", "data/", $url);
        $rdf_uri = str_replace(".rdf", "", $rdf_uri);
        $rdf_uri = $rdf_uri.'.rdf';
        return $rdf_uri;
    }
    
    public function generateWithLOM(string $search = '')
    {

        // Buscar con DBPedia Lookup API
        $dbpediaSearch = $this->dbpediaSearch($search);
        $response = [];

        if ($dbpediaSearch['success']) {
            $response = $dbpediaSearch['results'];
        } else {
            $message = $dbpediaSearch['message']?: "Error Processing Request";
            throw new \Exception($message, 1);
        }

        return $response;
    }

    
    public function getSparqlNamespaces()
    {
        EasyRdf_Namespace::set('dbc', 'http://dbpedia.org/resource/Category:');
        EasyRdf_Namespace::set('dbpedia', 'http://dbpedia.org/resource/');
        EasyRdf_Namespace::set('dbo', 'http://dbpedia.org/ontology/');
        EasyRdf_Namespace::set('dbp', 'http://dbpedia.org/property/');

        return EasyRdf_Namespace::namespaces();
    }

    public function getRdfData()
    {
        return $this->foaf->dump('html');
    }

    public function getResource($uri)
    {
        return isset($this->resources[str_replace('http://', 'https://', $this->uri)]) ? $this->resources[str_replace('http://', 'https://', $this->uri)] : $this->resources[str_replace('https://', 'http://', $this->uri)];
    }



    

    /* INICIO - OBTENCIÓN DE ATRIBUTOS */

    # Título del Oaa
    public function getTitle($lang = null)
    {
        $dbo = $this->foaf->resourcesMatching('dbo:abstract');
        $dbpedia_owl = $this->foaf->resourcesMatching('dbpedia-owl:abstract');
        $resource = empty($dbpedia_owl) ? $dbo[0]:$dbpedia_owl[0];
        $literals = $resource->allLiterals('skos:prefLabel|rdfs:label|foaf:name|rss:title|dc:title|dc11:title', $lang);
        
        foreach ($literals as $key => $literal) {
            if ($literal->getValue()) {
                return $literal->getValue();
            }
        }
    }
    public function getLanguage($lang = null)
    {
        if ($lang) {
            return $lang;
        }
        $dbo = $this->foaf->resourcesMatching('dbo:abstract');
        $dbpedia_owl = $this->foaf->resourcesMatching('dbpedia-owl:abstract');
        $resource = empty($dbpedia_owl) ? $dbo[0]:$dbpedia_owl[0];
        $literal = $resource->getLiteral('skos:prefLabel|rdfs:label|foaf:name|rss:title|dc:title|dc11:title');
        return  $literal->getLang();
    }

    public function getDescription($lang = null)
    {
        $dbo = $this->foaf->resourcesMatching('dbo:abstract');
        $dbpedia_owl = $this->foaf->resourcesMatching('dbpedia-owl:abstract');
        $resource = empty($dbpedia_owl) ? $dbo[0]:$dbpedia_owl[0];
        $literal = $resource->getLiteral('dbo:abstract', $lang);
        return  $literal->getValue();
    }

    public function getKeyword($lang = null)
    {
        $TextRazorClient = new TextRazorAnalyzing();
        $result = $TextRazorClient->analyzingUri($this->getLocation($lang));
        $result = to_object($result);
        if (isset($result->response)) {
            $collection = collect($result->response->entities);
            // $sorted = $collection->groupBy('entityId');

            $sorted = $collection->whereNotIn('matchedText', [$this->getTitle($lang)])
                        ->where('relevanceScore', '>=', 0.75)
                        ->sortByDesc(['confidenceScore']);
            $sorted = $sorted->values()->first();
            $results = $sorted->matchedText;
            return  $results;
        } else {
            return null;
        }
    }

    public function getKeywords($lang = null)
    {
        $dbo = $this->foaf->resourcesMatching('dbo:abstract');
        $dbpedia_owl = $this->foaf->resourcesMatching('dbpedia-owl:abstract');
        $resource = empty($dbpedia_owl) ? $dbo[0]:$dbpedia_owl[0];
        $types = $resource->all('rdf:type', 'resource');
        
        $result = [];
        foreach ($types as $key => $type) {
            if ($type->prefix() == 'dbpedia-owl' || $type->prefix() == 'dbo') {
                $type->load();
                array_push($result, $type->label($lang)->getValue());
            }
        }
        return  $result;
    }

    public function getLocation($lang = null)
    {
        $dbo = $this->foaf->resourcesMatching('dbo:abstract');
        $dbpedia_owl = $this->foaf->resourcesMatching('dbpedia-owl:abstract');
        $resource = empty($dbpedia_owl) ? $dbo[0]:$dbpedia_owl[0];

        return $resource->get('prov:wasDerivedFrom', 'resource', $lang)->getUri();
    }

    public function getFormat($lang = null)
    {
        try {
            $standard_resource = $this->getLocation($lang);
            $standard_resource = json_decode(json_encode(get_headers($standard_resource, 1)), true);
            $standard_resource = $standard_resource["Content-Type"];

            $standard_resource = explode(';', $standard_resource);
            $standard_resource = isset($standard_resource[0]) ? $standard_resource[0] : ' ';
            $standard_resource =  preg_replace("/[^A-Za-z0-9?![:space:]]/", " ", $standard_resource);

            $standard_resource = explode(' ', $standard_resource);
            $standard_resource = isset($standard_resource[1]) ? $standard_resource[1] : $standard_resource[0];

            $standard_resource = $standard_resource ? $standard_resource : null;
            return $standard_resource;
        } catch (\Throwable $th) {
        }
        return null;
    }


    # Obtener LOM OBJECT
    public function getLOM($lang = 'en')
    {
        # Nuevo Oaa
        $Oaa = new Oaa();

        # LOM General
        $Oaa->lom_general = new LomGeneral();
        $Oaa->lom_general->ambito = null;
        $Oaa->lom_general->titulo = mb_convert_encoding($this->getTitle($lang), 'UTF-8', 'UTF-8');
        $Oaa->lom_general->idioma = mb_convert_encoding($this->getLanguage($lang), 'UTF-8', 'UTF-8');
        $Oaa->lom_general->descripcion = mb_convert_encoding($this->getDescription($lang), 'UTF-8', 'UTF-8');
        $Oaa->lom_general->palabra_clave = mb_convert_encoding($this->getKeyword($lang), 'UTF-8', 'UTF-8');

        // # LOM Clasificación
        $Oaa->lom_clasificacion = new LomClasificacion();
        $Oaa->lom_clasificacion->descripcion = null;
        $Oaa->lom_clasificacion->palabras_claves = mb_convert_encoding($this->getKeywords($lang), 'UTF-8', 'UTF-8');
        $Oaa->lom_clasificacion->proposito = null;
        $Oaa->lom_clasificacion->ruta_tax_entrada = null;
        $Oaa->lom_clasificacion->ruta_tax_fuente = null;
        $Oaa->lom_clasificacion->ruta_tax_identificador = null;

        // # LOM Educativo
        $Oaa->lom_educativo = new LomEducativo();
        $Oaa->lom_educativo->contexto_uso = null;
        $Oaa->lom_educativo->densidad_semantica = null;
        $Oaa->lom_educativo->destinatario = null;
        $Oaa->lom_educativo->edad = null;
        $Oaa->lom_educativo->nivel_interactividad = null;
        $Oaa->lom_educativo->tiempo_aprendizaje = null;
        $Oaa->lom_educativo->tipo_interactividad = null;
        $Oaa->lom_educativo->tipo_recurso_educativo = null;

        // # LOM Técnica
        $Oaa->lom_tecnica = new LomTecnica();
        $Oaa->lom_tecnica->duracion = null;
        $Oaa->lom_tecnica->formato = mb_convert_encoding($this->getFormat(), 'UTF-8', 'UTF-8');
        $Oaa->lom_tecnica->localizacion = mb_convert_encoding($this->getLocation($lang), 'UTF-8', 'UTF-8');
        $Oaa->lom_tecnica->otros_requisitos = null;
        $Oaa->lom_tecnica->pautas_instalacion = null;
        $Oaa->lom_tecnica->tamano = null;

        return $Oaa;
    }

    /* FIN - OBTENCIÓN DE ATRIBUTOS */

    # Obtener dato según la propiedad...
    private function get($property, $type = null, $lang = null)
    {
        return strval($this->uri->get($property, $type, $lang));
    }
}


## Add namespaces
EasyRdf_Namespace::set('dbpedia', 'http://dbpedia.org/resource/');
EasyRdf_Namespace::set('dbpedia-owl', 'http://dbpedia.org/ontology/');
EasyRdf_Namespace::set('dbo', 'http://dbpedia.org/ontology/');
EasyRdf_Namespace::set('ns5', 'http://dbpedia.org/datatype/');
EasyRdf_Namespace::set('ns1', 'http://dbpedia.org/class/yago/');
EasyRdf_Namespace::set('dbp', 'http://dbpedia.org/property/');
EasyRdf_TypeMapper::set('dboa:DBPediaOa', 'DBPedia_OA');
