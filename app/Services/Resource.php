<?php

# La API TextRazor lo ayuda a extraer y comprender quién, qué, por qué de un texto/url
# API: https://www.textrazor.com/
# Plugin: https://github.com/TextRazor/textrazor-php

namespace App\Services;

use Illuminate\Database\Eloquent\Model;
use Storage;
use App\Services\TextRazorAnalyzing; // Servicio de analisis de texto
use App\Services\Translator; // Servicio de traducción
use App\Library\VoiceRss;
use Illuminate\Support\Facades\Log;

class Resource
{
    private $metadata = null;
    private $content = null;

    # Constructor
    public function __construct($metadata)
    {
        $this->metadata = to_object($metadata);
        $textRazorClient = new TextRazorAnalyzing();

        if (strpos($this->metadata->lom_tecnica->localizacion, $_SERVER['HTTP_HOST']) == false) {
            $text = $textRazorClient->getUrlText($this->metadata->lom_tecnica->localizacion);
        } else {
            $text =  strip_tags($this->getStorageHtml());
        }
        $this->metadata->lom_tecnica->localizacion = $this->storageHtml($text);
    }

    # Obtener metadatos
    public function getMetadata()
    {
        return $this->metadata;
    }

    # Obtener contenido html
    public function getContent()
    {
        $text = $this->getStorageHtml();
        return $text;
    }

    # Tranformar recurso
    public function transform(string $learning_style_default = 'texto')
    {

        // Actualizar metadatos
        $this->updateMetadata($this->metadata);

        // transformar recurso
        switch ($learning_style_default) {
            case 'Aural':
                Log::info('transformacion de texto a audio');

                $content = $this->getStorageHtml();

                $tts = new VoiceRSS;
                $voice = $tts->speech([
                    'key' => env('VOICE_RSS_API_KEY'),
                    'hl' => 'en-us',
                    'src' => $content,
                    'r' => '0',
                    'c' => 'mp3',
                    'f' => '44khz_16bit_stereo',
                    'ssml' => 'false',
                    'b64' => 'false'
                ]);

                Storage::disk('speeches')->put($this->metadata->id . '/oaa.mp3', $voice['response']);

                $url = Storage::disk('speeches')->url($this->metadata->id . '/oaa.mp3');
                // $url = str_replace(' ', '', $url);

                $this->metadata->lom_tecnica->localizacion = $url;
                $this->metadata->lom_tecnica->formato = 'mp3';
                break;
        }
    }

    private function storageHtml($text = '')
    {
        $folder = $this->metadata->id;
        $content = $text;
        Storage::disk('public')->put($folder . '/resource.html', $content);
        $url = Storage::disk('public')->url($folder . '/resource.html');
        $url = str_replace(' ', '', $url);
        return $url;
    }

    private function getStorageHtml()
    {
        $folder = $this->metadata->id;
        $content = Storage::disk('public')->get($folder . '/resource.html');
        return $content;
    }

    public function updateMetadata($oaa)
    {
        $content = $this->getStorageHtml();

        $this->metadata->lom_general->ambito = $oaa->lom_general->ambito;
        $this->metadata->lom_general->titulo = $oaa->lom_general->titulo;
        $this->metadata->lom_general->idioma = $oaa->lom_general->idioma;
        $this->metadata->lom_general->descripcion = $oaa->lom_general->descripcion;
        $this->metadata->lom_general->palabra_clave = $oaa->lom_general->palabra_clave;

        $this->metadata->lom_clasificacion->descripcion = $oaa->lom_clasificacion->descripcion;
        $this->metadata->lom_clasificacion->palabras_claves = $oaa->lom_clasificacion->palabras_claves;
        $this->metadata->lom_clasificacion->proposito = $oaa->lom_clasificacion->proposito;

        $this->metadata->lom_educativo->contexto_uso = $oaa->lom_educativo->contexto_uso;
        $this->metadata->lom_educativo->densidad_semantica =  $oaa->lom_educativo->densidad_semantica;
        $this->metadata->lom_educativo->destinatario = $oaa->lom_educativo->destinatario;

        $this->metadata->lom_educativo->nivel_interactividad = $oaa->lom_educativo->nivel_interactividad;
        $this->metadata->lom_educativo->tiempo_aprendizaje =  $oaa->lom_educativo->tiempo_aprendizaje;
        $this->metadata->lom_educativo->tipo_interactividad =  $oaa->lom_educativo->tipo_interactividad;
        $this->metadata->lom_educativo->tipo_recurso_educativo = $oaa->lom_educativo->tipo_recurso_educativo;
        $this->metadata->lom_tecnica->localizacion = str_replace(' ', '', $oaa->lom_tecnica->localizacion);
        $this->metadata->lom_tecnica->otros_requisitos = $oaa->lom_tecnica->otros_requisitos;
        $this->metadata->lom_tecnica->pautas_instalacion = $oaa->lom_tecnica->pautas_instalacion;

        $this->metadata->lom_tecnica->localizacion = $this->storageHtml($content);

        return $this->metadata;
    }
}
