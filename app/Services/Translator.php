<?php

# Traductor
# API: https://languagelayer.com
# Plugin: https://github.com/BenMorel/LanguageLayer

namespace App\Services;

use Illuminate\Database\Eloquent\Model;
use \Statickidz\GoogleTranslate;

class Translator
{

    # Constructor
    public function __construct()
    {
    }

    # Traductor de texto multigestor (En prioridad: Google Traductor, Microsoft Traductor)
    public function translateText(string $text, string $lang_output, string $lang_input)
    {
        $response = '';

        // Buscar con Microsoft Traductor
        if (empty($response)) {
            $response = $this->translateMicrosoft($text, $lang_output, $lang_input);
        }

        $response = $response && str_contains($response, 'must be a valid language') ? null : $response;

        // Buscar con Google Traductor
        if (empty($response)) {
            $response = $this->translateGoogle($text, $lang_output, $lang_input);
        }
        
        $response = $response && str_contains($response, 'must be a valid language') ? null : $response;

        return $response ? $response : '';

    }

    # Traductor de json multigestor (En prioridad: Google Traductor, Microsoft Traductor)
    public function translateJson(object $json, string $lang_output, string $lang_input)
    {
        $json_result = $json;

        foreach ($json_result as $key => $value) {
            if ($value && $key) {
                if (!is_numeric($value) && !is_null($value)) {
                    if (gettype($value) == 'object') {
                        foreach ($value as $jkey => $val) {
                            if (!is_numeric($val) && gettype($val) == 'string') {
                                sleep(0.5);

                                $json_result->$key->$jkey = $this->translateText($val, $lang_output, $lang_input);
                            }
                        }
                    } elseif (gettype($value) == 'string') {
                        sleep(0.5);

                        $json_result->$key = $this->translateText($value, $lang_output, $lang_input);
                    }
                }
            }
        }

        return $json_result;
    }

    # Traducir un texto a un idioma destino con Google Traductor
    public function translateGoogle(string $text, string $lang_output, string $lang_input)
    {
        try {
            $characters_limit = 4500;
            $textArray = characters_split_at_lastword($text, $characters_limit);

            $client = new GoogleTranslate();
            $result = '';
            foreach ($textArray as $key => $textFragment) {
                $translate = $client->translate($lang_input, $lang_output, $textFragment);
                $result = $result . ' ' . $translate;
                $result = trim(str_replace(' ??"', '', mb_convert_encoding($result, 'UTF-8', 'UTF-8')));
                sleep(0.5);
            }
            return $result;
        } catch (\Throwable $th) {
            return '';
        }
    }

    # Traducir un texto a un idioma destino con Microsoft Traductor
    public function translateMicrosoft(string $text, string $lang_output, string $lang_input)
    {
        try {
            $characters_limit = 10000;
            $textArray = characters_split_at_lastword($text, $characters_limit);
            $result = '';
            $appId = 'DB50E2E9FBE2E92B103E696DCF4E3E512A8826FB';
            $appId2 = '58C40548A812ED699C35664525D8A8104D3006D2';
            foreach ($textArray as $key => $textFragment) {
                $url = 'https://api.microsofttranslator.com/V2/Ajax.svc/Translate?appId='.$appId.'&oncomplete=?&text='.urlencode($textFragment).'&from='.$lang_input.'&to='.$lang_output;
                // echo $url;
                $translate = substr(file_get_contents($url), 1, -1); // Elimnar el " del inicio y final sel texto
                $result = $result . ' ' . $translate;
                $result = trim(str_replace(' ??"', '', mb_convert_encoding($result, 'UTF-8', 'UTF-8')));
                sleep(0.5);
            }
            
            return $result;
        } catch (\Throwable $th) {
            return '';
        }
    }

    private function decode_translate_json($encode_json, $json = null)
    {
        $decode  = $encode_json;
        // $decode  = str_replace('nulo', 'null', $decode);
        // $decode  = str_replace('�', '', $decode);
        // $decode  = str_replace('  ', ' ', $decode);
        // $decode  = str_replace('  ', ' ', $decode);
        // $decode  = str_replace('  ', ' ', $decode);
        // $decode  = str_replace(' "', '"', $decode);
        // $decode  = str_replace('" ', '"', $decode);
        // $decode  = str_replace(' {', '{', $decode);
        // $decode  = str_replace('{ ', '{', $decode);
        // $decode  = str_replace(' }', '}', $decode);
        // $decode  = str_replace('} ', '}', $decode);
        // $decode  = str_replace(' /', '/', $decode);
        // $decode  = str_replace('/ ', '/', $decode);
        // $decode  = str_replace('\ /', '/', $decode);
        // $decode  = str_replace(' \ u', '\u', $decode);
        // $decode  = str_replace(' :', ':', $decode);
        // $decode  = str_replace(' ;', ';', $decode);
        // $decode  = str_replace(' .', '.', $decode);
        // $decode  = str_replace(' ,', ',', $decode);
        // $decode  = str_replace('=', '', $decode);

        return $result = $decode;

        return $arrayData =  explode('-CU7H3R3-', $result);
         
        return $result  = json_decode($decode);

        if ($json) {
            // Por editar
        }

        return $result;
    }
}
