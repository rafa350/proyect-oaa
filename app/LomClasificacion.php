<?php

// Esta  subclase descrribe dónde se sitúa el objeto educativo dentro de un sistema de clasificación concreto.
// Corresponde a la categoría 9 del standard LOM
namespace App;

use App\Enums\Lom\ClasificationEnums;
use Illuminate\Database\Eloquent\Model;
use App\Oaa;

class LomClasificacion extends Model implements ClasificationEnums
{

    protected $table = self::BD_TABLE;

    protected $primaryKey = self::ID;

    protected $fillable = [
        self::DESCRIPTION,
        self::WORD_KEYS,
        self::PURPOSE,
        self::INPUT_TAX,
        self::ORIGIN_TAX,
        self::ID_TAX,
    ];

    protected $casts = [self::WORD_KEYS => 'array'];


    public function oaa()
    {
        return $this->belongsTo(Oaa::class, 'oaa_id', 'id');

    }//end oaa()


}//end class
