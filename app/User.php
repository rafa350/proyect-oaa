<?php

// Identifica al usuario que interactúa en el proceso de enseñanza/aprendizaje
namespace App;

use  App\Enums\UserEnums;
use App\Enums\Data\DataTypeEnums;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements UserEnums, JWTSubject
{
    use Notifiable;

    protected $primaryKey = self::ID;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::NAME,
        self::EMAIL,
        self::PASSWORD,
        self::SECURITY_QUESTION,
        self::SEQURITY_ANSWER,
        self::GENDER,
        self::BIRTHDATE,
        self::BIRTHPLACE,
        self::STATUS,
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        self::PASSWORD,
        self::REMEMBER_TOKEN,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        self::NAME              => DataTypeEnums::STRING_TYPE,
        self::EMAIL             => DataTypeEnums::STRING_TYPE,
        self::SECURITY_QUESTION => DataTypeEnums::STRING_TYPE,
        self::SEQURITY_ANSWER   => DataTypeEnums::STRING_TYPE,
        self::GENDER            => DataTypeEnums::STRING_TYPE,
        self::BIRTHDATE         => DataTypeEnums::DATETIME_TYPE,
        self::BIRTHPLACE        => DataTypeEnums::STRING_TYPE,
        self::STATUS            => DataTypeEnums::BOOLEAN_TYPE,
        self::EMAIL_VERIFIED_AT => DataTypeEnums::DATETIME_TYPE,
    ];


    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();

    }//end getJWTIdentifier()


    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];

    }//end getJWTCustomClaims()


    public function preferencias()
    {
        return $this->hasOne(Preferencias::class);

    }//end preferencias()


    public function log_oaas()
    {
        return $this->hasMany(LogOaa::class);

    }//end log_oaas()


    public function log_consultas()
    {
        return $this->hasMany(LogConsulta::class);

    }//end log_consultas()


}//end class
