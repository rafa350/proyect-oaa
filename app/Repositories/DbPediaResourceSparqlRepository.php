<?php

namespace App\Repositories;

use \BorderCloud\SPARQL\SparqlClient;
use App\Enums\DbPediaResourceSparqlEnums;

class DbPediaResourceSparqlRepository implements DbPediaResourceSparqlEnums
{


    /**
     * DbPediaResourceSparqlRepository constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $endpoint = "https://dbpedia.org/sparql";
        $this->DbPediaResourceSparql    = new SparqlClient();
        $this->DbPediaResourceSparql->setEndpointRead($endpoint);
    } //end __construct()


    /**
     * Get query.
     *
     * @param string $search Search
     * @param string $lang Language
     * @param integer $limit Number of items
     *
     * @return string
     */
    public function query($search, $lang = "en", $limit = 10)
    {
        $prefix = self::PREFIX;
        $search = explode(' ', $search);
        $contains = '';
        foreach ($search as $word) {
            $contains = "{$contains}  OPTIONAL { filter contains(?abstract,'{$word}'). }";
        }
        return    "{$prefix}
        SELECT Distinct *
        WHERE {
            ?resource rdfs:label ?label.
            ?resource rdfs:comment ?comment.
            ?resource dbo:abstract ?abstract. 
            ?resource dbo:thumbnail  ?thumbnail.
            Filter langMatches(lang(?label),'{$lang}').  
            Filter langMatches(lang(?comment),'{$lang}'). 
            Filter langMatches(lang(?abstract),'{$lang}'). 
            ?label bif:contains '{$search[0]}'. 
            {$contains}
        }
       limit {$limit}";
    } //end query()


    // end getAll()


    /**
     * Get all Db Pedia Resource Sparql records.
     *
     * @param array $data Filters data
     *
     * @return Collection
     */
    public function getAll($data = [])
    {
        $query = $this->DbPediaResourceSparql->query($this->query($data['q'] ?? null), 'rows');
        $results = isset($query['result']) ?  ($query['result']['rows'] ?? []) : [];
        $results = to_array($results);
        return collect($results)->map(function ($result) {
            return collect($result);
        });
    } //end getAll()


    /**
     * Get query.
     *
     * @param string $search Search
     * @param string $lang Language
     * @param integer $limit Number of items
     *
     * @return string
     */
    public function getInstance($uri, $lang = "en", $limit = 1)
    {
        $prefix = self::PREFIX;
        return    "{$prefix}
        
        SELECT ?label ?abstract ?description ?location ?thumbnail
        (GROUP_CONCAT(DISTINCT ?_keywords ; separator=', ') AS ?keywords) 
        '{$lang}' AS ?lang
        WHERE 
        { 
            <{$uri}> rdfs:label ?label.
            <{$uri}> dbo:abstract ?abstract.
            <{$uri}> rdfs:comment ?description.
            <{$uri}> dbo:thumbnail  ?thumbnail.
            <{$uri}> prov:wasDerivedFrom ?location.
        OPTIONAL
        { 
            <{$uri}>  rdf:type/rdfs:label  ?_keywords
             Filter langMatches(lang(?_keywords),'{$lang}').  
        }
        Filter langMatches(lang(?label),'{$lang}').  
        Filter langMatches(lang(?abstract),'{$lang}').  
        Filter langMatches(lang(?description),'{$lang}').  
        }
        limit {$limit}";
    } //end query()


    /**
     * Get all Db Pedia Resource Sparql records.
     *
     * @param array $data Filters data
     *
     * @return object
     */
    public function showDbPediaResource($data = [])
    {
        $query = $this->DbPediaResourceSparql->query($this->getInstance($data['uri'] ?? null), 'rows');
        $results = isset($query['result']) ?  ($query['result']['rows'] ?? []) : [];
        $results = to_array($results);
        return isset($results[0]) ? [
            'uri' => $data['uri'],
            'label' => $results[0]['label'],
            'abstract' => $results[0]['abstract'],
            'description' => $results[0]['description'],
            'location' => $results[0]['location'],
            'thumbnail' => $results[0]['thumbnail'],
            'keywords' => explode(', ', $results[0]['keywords']),
            'lang' => $results[0]['lang'],
        ] : null;
    } //end getAll()




}//end class
