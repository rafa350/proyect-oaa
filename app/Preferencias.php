<?php

// Especifíca las preferencias del aprendiz
namespace App;

use App\Enums\Data\DataTypeEnums;
use App\Enums\PreferenceEnums;
use Illuminate\Database\Eloquent\Model;
use App\User;

class Preferencias extends Model implements PreferenceEnums
{

    protected $table = self::BD_TABLE;

    protected $primaryKey = self::ID;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::USER_ID,
        self::CONTENT,
        self::LANGUAGE,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        self::USER_ID  => DataTypeEnums::INTEGER_TYPE,
        self::CONTENT  => DataTypeEnums::STRING_TYPE,
        self::LANGUAGE => DataTypeEnums::STRING_TYPE,
    ];


    public function user()
    {
        return $this->belongsTo(User::class, self::USER_ID, User::ID);

    }//end user()


}//end class
