<?php

namespace App;

use App\Enums\Data\DataTypeEnums;
use App\Enums\LogQueryEnums;
use Illuminate\Database\Eloquent\Model;

class LogConsulta extends Model implements LogQueryEnums
{

    protected $table = self::BD_TABLE;

    protected $primaryKey = self::ID;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::VALUE,
        self::LANGUAGE,
        self::USER_ID,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        self::VALUE    => DataTypeEnums::STRING_TYPE,
        self::LANGUAGE => DataTypeEnums::STRING_TYPE,
        self::USER_ID  => DataTypeEnums::INTEGER_TYPE,
    ];


    public function user()
    {
        return $this->belongsTo(User::class, self::USER_ID, User::ID);

    }//end user()


}//end class
