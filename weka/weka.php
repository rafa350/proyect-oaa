<?php

if (!function_exists('artisan')) {
    function artisan()
    {
        return app()->make('Illuminate\Contracts\Console\Kernel');
    }
}

$model = "model_phone.model"; // Localización del JAR de Weka

//  'app_path()' => app_path(),
//  'storage_path()' => storage_path(),
//  'base_path()' => base_path(),

// Localización del JAR de Weka
function get_weka_jar()
{
    $locate_jar = base_path() . "\\weka\\weka.jar";
    $locate_jar = "weka.jar";
    return $locate_jar;
}

function get_weka_source($source)
{
    if (app()->environment(['local'])) {
        return Storage::disk('weka')->path($source);
    } else {
        return "$source";
    }
}

function connect($source)
{
    $locate_jar = $this->locate_jar;
    // Transformar a Arff
    $cmd = 'java -classpath "' . $locate_jar . '" weka.core.converters.URLSourcedLoader ' . $source . ' > phone_unseen.arff';
    exec($cmd, $output);

    $cmd1 = 'java -classpath "' . $locate_jar . '" weka.classifiers.trees.J48 -T "phone_unseen.arff" -l "model_phone.model" -p 5';
    exec($cmd1, $output1);
}

function classifiers(string $source, $indexClass = 0)
{

    // Verificar index
    $indexClass = $indexClass >= 0 ? $indexClass : 0;

    //  $indexClass = 0;
    // Tranformación a ARFF
    $command = 'java -classpath "' . get_weka_jar() . '" weka.core.converters.CSVLoader "' . get_weka_source($source) . '" > "' . get_weka_source($source) . '.arff"';
    $output = exec_c($command);

    // Creación de Modelo
    $command = 'java -classpath "' . get_weka_jar() . '" weka.classifiers.trees.RandomForest -t "' . get_weka_source($source) . '.arff" -d "' . get_weka_source($source) . '.model" -p ' . $indexClass;
    $output = exec_c($command);

    // Ejecutar Método
    $command = 'java -classpath "' . get_weka_jar() . '" weka.classifiers.trees.RandomForest -T "' . get_weka_source($source) . '.arff" -l "' . get_weka_source($source) . '.model" -p ' . $indexClass;
    $output = exec_c($command);


    //  return $output;
    $resultIndex = sizeof($output) - 3;
    if (isset($output[$resultIndex])) {
        $returnValue = explode(':', $output[$resultIndex]);

        while ($resultIndex && !isset($returnValue[2])) {
            $resultIndex--;
            $returnValue = explode(':', $output[$resultIndex]);
        }
        if ($resultIndex > 0) {
            $returnValues = explode(" ", $returnValue[2]);

            return $package = $returnValues[0];
        }
    }
    return $output;
}

function classifiersAlt(string $source, $indexClass = 0)
{

    // Verificar index
    $indexClass = $indexClass >= 0 ? $indexClass : 0;

    //  $indexClass = 0;
    // Tranformación a ARFF
    $command = 'java -classpath "' . get_weka_jar() . '" weka.core.converters.CSVLoader "' . get_weka_source($source) . '" > "' . get_weka_source($source) . '.arff"';
    $output = exec_c($command);

    // Creación de Modelo
    $command = 'java -classpath "' . get_weka_jar() . '" weka.classifiers.trees.J48 -t "' . get_weka_source($source) . '.arff" -d "' . get_weka_source($source) . '.model" -p ' . $indexClass;
    $output = exec_c($command);

    // Ejecutar Método
    $command = 'java -classpath "' . get_weka_jar() . '" weka.classifiers.trees.J48 -T "' . get_weka_source($source) . '.arff" -l "' . get_weka_source($source) . '.model" -p ' . $indexClass;
    $output = exec_c($command);


    //  return $output;
    $resultIndex = sizeof($output) - 3;
    if (isset($output[$resultIndex])) {
        $returnValue = explode(':', $output[$resultIndex]);

        while ($resultIndex && !isset($returnValue[2])) {
            $resultIndex--;
            $returnValue = explode(':', $output[$resultIndex]);
        }
        if ($resultIndex > 0) {
            $returnValues = explode(" ", $returnValue[2]);

            return $package = $returnValues[0];
        }
    }
    return $output;
}

function selectAttributes(string $source, $indexClass = 0)
{

    // Verificar index
    $indexClass = $indexClass >= 0 ? $indexClass : 0;
    //  $indexClass = 0;
    // Tranformación a ARFF
    $command = 'java -classpath "' . get_weka_jar() . '" weka.core.converters.CSVLoader "' . get_weka_source($source) . '" > "' . get_weka_source($source) . '.arff"';
    $output = exec_c($command);

    //  // Creación de Modelo
    //  $command = 'java -classpath "'. get_weka_jar() .'" weka.attributeSelection.ClassifierAttributeEval -s "weka.attributeSelection.Ranker -t "'. get_weka_source($source) .'.arff" -d "'.get_weka_source($source).'.model" -N ' . $indexClass;
    //  $output = exec_c($command);

    // Ejecutar Método
    $command = 'java -classpath "' . get_weka_jar() . '" weka.attributeSelection.ClassifierAttributeEval -s "weka.attributeSelection.Ranker -T -1.7976931348623157E308 -N -1" -i "' . get_weka_source($source) . '.arff"';
    $output = exec_c($command);
    // java -classpath "C:/Users/User/Dev/Trabajo de grado/oaa-warehouse-api/weka/weka.jar" weka.attributeSelection.InfoGainAttributeEval -s "weka.attributeSelection.Ranker -T -1.7976931348623157E308 -N -1" -i "C:/Users/User/Dev/Trabajo de grado/oaa-warehouse-api/storage/app/weka/1/log-oaas-1.csv.arff"

    //  return $output;
    $resultIndex = 0;
    $rank = [];
    $flag = true;
    while (str_contains($output[$resultIndex] ?? '', 'Ranked attributes') === false &&  $resultIndex < sizeof($output)) {
        $resultIndex++;
    }
    $resultIndex++;
    for ($i = $resultIndex; $flag && $i < sizeof($output); $i++) {
        $returnValue = explode(' ', $output[$i]);
        if (sizeof($returnValue) > 1) {
            $package = $returnValue[sizeof($returnValue) - 1];
            $rank[] = $package;
        } else {
            $flag = false;
        }
    }
    return $rank;
}


function exec_c($command)
{
    // Ejecutar comando
    $consoleOutput = shell_exec('nohup ' . $command . ' 2>&1 &');
    sleep(1);

    // Dividir por salto de linea
    $output = preg_split('/\r\n|\r|\n/', $consoleOutput);

    //Limpiar cada cadena
    foreach ($output as $key => &$line) {
        $output[$key] = trim($line);
    }

    return $output;
}
